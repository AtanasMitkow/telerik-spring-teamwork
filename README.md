# Telerik Spring Teamwork

Module 3 Telerik Spring framework project - BeerTag application

Trello board:
https://trello.com/b/ZafOfeqT

Documentation:
https://drive.google.com/file/d/11K_cLdMa7ltY3LSHi0AIFjSeo0F2hf37/view?usp=sharing

Database diagram:
https://photos.app.goo.gl/tuE1BWBrr57nCiHa6
![Diagram](beer_project/src/main/resources/static/img/beerDatabase-2019-07-04_15_51.png)

Register page :
![Register](beer_project/src/main/resources/static/img/66191485_371184630248072_8536532970096295936_n.png)

Login page:
![Login](beer_project/src/main/resources/static/img/66394477_443019329886217_1839708722520129536_n.png)

Guest Home Page :
![Login Home Page](beer_project/src/main/resources/static/img/66488202_1420525204762093_7313567429429297152_n.png)

UserHome Page:
![User Home Page](beer_project/src/main/resources/static/img/66839346_2344279282481407_1139128711056982016_n.png)

User Profile:
![User Profile](beer_project/src/main/resources/static/img/64470824_316565929224426_8293678180397481984_n.png)

Beer Profile:
![Beer Profile](beer_project/src/main/resources/static/img/66426143_349373505736133_1394297150518591488_n.png)

