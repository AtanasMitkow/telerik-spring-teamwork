package com.example.beer_project.mockobjects;

import com.example.beer_project.models.utility.Country;
import com.example.beer_project.models.utility.Style;
import com.example.beer_project.repositories.utility.CountryRepository;
import com.example.beer_project.repositories.utility.StyleRepository;
import com.example.beer_project.services.UtilityServiceImpl;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.*;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.ArrayList;
import java.util.Arrays;

@RunWith(MockitoJUnitRunner.class)
public class UtilityServiceImplTests {

    @Mock
    private CountryRepository countryRepository;

    @Mock
    private StyleRepository styleRepository;

    @Spy
    @InjectMocks
    private UtilityServiceImpl service;

    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void getAllCountries_Should_return_countries()
    {
        Mockito.when(countryRepository.getAll()).thenReturn(Arrays.asList(
                new Country(1,"Bulgaria"),
                new Country(2,"Greece"),
                new Country(3,"Romania")
        ));
        service.getAllCountries();
        Assert.assertEquals(3,service.getAllCountries().size());
    }

    @Test
    public void getAllCountries_Should_return_empty_list()
    {
        Mockito.when(countryRepository.getAll()).thenReturn(new ArrayList<>());
        service.getAllCountries();
        Assert.assertTrue(service.getAllCountries().isEmpty());
    }

    @Test
    public void getAllStyles_Should_return_styles()
    {
        Mockito.when(styleRepository.getAll()).thenReturn(Arrays.asList(
                new Style(1,"Pale"),
                new Style(2,"Ale"),
                new Style(3,"Blond")
        ));
        service.getAllStyles();
        Assert.assertEquals(3,service.getAllStyles().size());
    }

    @Test
    public void getAllStyles_Should_return_empty_list()
    {
        Mockito.when(styleRepository.getAll()).thenReturn(new ArrayList<>());
        service.getAllStyles();
        Assert.assertTrue(service.getAllStyles().isEmpty());
    }

}
