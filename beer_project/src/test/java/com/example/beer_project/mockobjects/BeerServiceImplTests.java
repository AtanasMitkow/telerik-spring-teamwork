package com.example.beer_project.mockobjects;

import com.example.beer_project.DTO.BeerDTO;
import com.example.beer_project.DTO.BeerDisplayDTO;
import com.example.beer_project.DTO.BeerUpdateDTO;
import com.example.beer_project.exceptions.*;
import com.example.beer_project.models.Beer;
import com.example.beer_project.models.BeerDetails;
import com.example.beer_project.models.User;
import com.example.beer_project.models.UserDetails;
import com.example.beer_project.models.utility.Country;
import com.example.beer_project.models.utility.Picture;
import com.example.beer_project.models.utility.Style;
import com.example.beer_project.repositories.BeerRepository;
import com.example.beer_project.repositories.UserRepository;
import com.example.beer_project.repositories.utility.CountryRepository;
import com.example.beer_project.repositories.utility.StyleRepository;
import com.example.beer_project.services.BeerServiceImpl;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.*;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.ArrayList;
import java.util.Arrays;

import static org.mockito.Mockito.*;
import static org.mockito.internal.verification.VerificationModeFactory.times;

@RunWith(MockitoJUnitRunner.class)
public class BeerServiceImplTests {

    @Mock
    private BeerRepository repositoryMock;

    @Mock
    private UserRepository userRepository;

    @Mock
    private CountryRepository countryRepository;

    @Mock
    private StyleRepository styleRepository;


    private Beer beer;
    private User user;
    private BeerDetails beerDetails;
    private Country country;
    private Style style;
    private BeerDTO beerDTO;
    private Picture picture;
    private BeerUpdateDTO beerUpdateDTO;
    private BeerDisplayDTO beerDisplayDTO;

    @Spy
    @InjectMocks
    private BeerServiceImpl service;


    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);

        this.country = new Country(1, "Bulgaria");
        this.style = new Style(1, "Svetlo");
        this.picture = new Picture(1, "picture");
        this.beerDetails = new BeerDetails(1, 2, "brewery", country, style);
        this.beerDTO = new BeerDTO("beer", "descr", "tagche", "picture", "user1", 5, "brewery", "Bulgaria", "Svetlo", 3);
        this.user = new User("user1", "password", "user1@gmail.com", "ROLE_USER", true, new UserDetails());
        this.user.setId(1);
        this.beerUpdateDTO = new BeerUpdateDTO("beer", "picture", 5, "brewery");
        this.beerDisplayDTO = new BeerDisplayDTO("beer", "picture", 5, 1, 3, "descr", "user1");
        this.beer = new Beer(1, "beer", "descr", picture, beerDetails, user, false, 1);
    }

    @Test
    public void getByID_Should_Return_Beer_When_Beer_With_Same_ID_Exists() throws BeerDoesntExistException {
        Mockito.when(repositoryMock.getById(1)).thenReturn(beer);
        service.getById(1);
        Assert.assertEquals(beer, service.getById(1));
    }

    @Test(expected = IllegalArgumentException.class)
    public void getByID_Should_Return_Null_When_Beer_With_ID_Doesnt_Exists() throws BeerDoesntExistException {
        Mockito.when(repositoryMock.getById(2)).thenThrow(new IllegalArgumentException());
        service.getById(2);
        Assert.assertNull(service.getById(2));
    }

    @Test
    public void getAll_Should_Return_Empty_List_When_There_Is_No_Beers() {
        Mockito.when(repositoryMock.getAll()).thenReturn(new ArrayList<>());
        service.getAll();
        Assert.assertTrue(repositoryMock.getAll().isEmpty());
    }

    @Test
    public void getAll_Should_Return_List_When_Have_Beers() {
        Mockito.when(repositoryMock.getAll()).thenReturn(Arrays.asList(
                new Beer(),
                new Beer(),
                new Beer()
        ));

        service.getAll();
        Assert.assertEquals(3, service.getAll().size());
    }

    @Test
    public void GetByBeerName_Should_Return_Beer_When_Exists() throws BeerDoesntExistException {
        beer.setName("name");
        Mockito.when(repositoryMock.getByBeerName("name")).thenReturn(beer);
        service.getByBeerName("name");
        Assert.assertEquals(beer.getName(), service.getByBeerName("name").getName());
    }

    @Test(expected = BeerDoesntExistException.class)
    public void GetByBeerName_Should_Throw_Exception_When_Beer_Doesnt_Exists() throws BeerDoesntExistException {
        Mockito.when(repositoryMock.getByBeerName("name1")).thenReturn(null);
        service.getByBeerName("name1");
    }

    @Test(expected = BeerDoesntExistException.class)
    public void update_should_throw_exception_when_beer_doesnt_exist() throws BeerDoesntExistException {
        BeerUpdateDTO beerUpdateDTO = new BeerUpdateDTO();
        service.update(beer.getName(), beerUpdateDTO);
        verify(repositoryMock, Mockito.times(0)).update(3, beer);
    }

    @Test
    public void update_should_update_when_beer_exist() throws BeerDoesntExistException {
        Mockito.when(repositoryMock.getByBeerName("beer")).thenReturn(beer);
        beer.setId(1);
        service.update(beer.getName(), beerUpdateDTO);
        verify(repositoryMock, times(1)).update(1, beer);
    }

    @Test(expected = BeerDoesntExistException.class)
    public void delete_should_throw_exception_when_beer_doesnt_exist() throws BeerDoesntExistException {
        Mockito.when(repositoryMock.getById(3)).thenReturn(null);
        service.delete(3);
        verify(repositoryMock, Mockito.times(0)).delete(3);
    }


    @Test
    public void delete_should_delete_when_beer_exist() throws BeerDoesntExistException {
        Mockito.when(repositoryMock.getById(1)).thenReturn(beer);
        service.delete(1);
        verify(repositoryMock, times(1)).delete(1);
    }

    @Test
    public void create_Should_Create_New_Beer() throws BeerWithNameAlreadyExistException, BeerDoesntExistException {
        Mockito.when(countryRepository.getByName("Bulgaria")).thenReturn(country);
        Mockito.when(styleRepository.getByName("Svetlo")).thenReturn(style);
        Mockito.when(repositoryMock.getByBeerName("beer")).thenReturn(null);
        doNothing().when(service).addTagToTagList("tagche", "beer");
        service.create(beerDTO);
        verify(service, Mockito.times(1)).create(beerDTO);
    }

    @Test(expected = BeerWithNameAlreadyExistException.class)
    public void create_Should_Throw_Exception_When_Beer_Already_Exists() throws BeerWithNameAlreadyExistException, BeerDoesntExistException {
        Mockito.when(repositoryMock.getByBeerName("beer")).thenReturn(beer);
        service.create(beerDTO);
        verify(repositoryMock, Mockito.times(0)).create(beer);
    }

    @Test
    public void beerToDisplayDTO_Should_Return_DTO() {
        doReturn(beerDisplayDTO).when(service).beerToDisplayDTO(beer);
        service.beerToDisplayDTO(beer);
        verify(service, times(1)).beerToDisplayDTO(beer);
    }

    @Test
    public void listBeers_Should_Return_Beers() {
        Mockito.when(repositoryMock.getMaxPagesByCriteria("")).thenReturn((long) 8);
        Mockito.when(repositoryMock.getPage(1, "", " order")).thenReturn(Arrays.asList(
                new Beer(2, "beer2", "descr", picture, beerDetails, user, false, 1),
                new Beer(3, "beer3", "descr", picture, beerDetails, user, false, 1),
                new Beer(4, "beer4", "descr", picture, beerDetails, user, false, 1)
        ));

        service.listBeers(1, "", "(order)");
        Assert.assertEquals(3, service.listBeers(1, "", "(order)").size());
    }

    @Test(expected = IllegalArgumentException.class)
    public void listBeers_Should_Throw_Exception_When_Page_Is_Invalid() {
        service.listBeers(3, "", "");
    }

    @Test
    public void listBeers_should_return_empty_list()
    {
        Mockito.when(repositoryMock.getMaxPagesByCriteria("")).thenReturn((long) 8);
        Mockito.when(repositoryMock.getPage(1, "", " order")).thenReturn(new ArrayList<>());
        service.listBeers(1, "", "(order)");
        Assert.assertTrue(service.listBeers(1,"","(order)").isEmpty());
    }


    @Test
    public void getMaxPages_Should_Return_Number() {
        Mockito.when(repositoryMock.getMaxPagesByCriteria("")).thenReturn((long) 8);
        service.getMaxPages("");
        Assert.assertEquals(2, (long) service.getMaxPages(""));
    }

    @Test
    public void beerToDTO_Should_Return_DTO() {

        service.beerToDTO(beer);
        verify(service, times(1)).beerToDTO(beer);
    }

    @Test(expected = BeerDoesntExistException.class)
    public void addTagToTagList_Should_Throw_Exception_When_Beer_doesnt_Exist() throws BeerDoesntExistException {
        Mockito.when(repositoryMock.getByBeerName("beer1")).thenReturn(null);
        service.addTagToTagList("tagche", "beer1");
    }

    @Test
    public void addTagToTagList_Should_Add_Tag_When_Beer_Exist() throws BeerDoesntExistException {
        Mockito.when(repositoryMock.getByBeerName("beer")).thenReturn(beer);
        service.addTagToTagList("tagche", "beer");
        verify(repositoryMock, times(1)).update(1, beer);
    }

    @Test(expected = BeerDoesntExistException.class)
    public void rate_should_throw_exception_when_beer_doesnt_exist() throws UserDoesntExistException, RatingRangeException, BeerDoesntExistException, CantRateNotTastedBeerException {
        Mockito.when(repositoryMock.getByBeerName("beer1")).thenReturn(null);
        service.rate("beer1", 1, 2);
    }

    @Test(expected = UserDoesntExistException.class)
    public void rate_should_throw_exception_when_user_doesnt_exist() throws UserDoesntExistException, RatingRangeException, BeerDoesntExistException, CantRateNotTastedBeerException {
        Mockito.when(repositoryMock.getByBeerName("beer")).thenReturn(beer);
        Mockito.when(userRepository.getById(1)).thenReturn(null);
        service.rate("beer", 1, 2);
    }

    @Test(expected = RatingRangeException.class)
    public void rate_should_throw_exception_when_is_given_incorrect_rating() throws UserDoesntExistException, RatingRangeException, BeerDoesntExistException, CantRateNotTastedBeerException {
        Mockito.when(repositoryMock.getByBeerName("beer")).thenReturn(beer);
        Mockito.when(userRepository.getById(1)).thenReturn(user);
        service.rate("beer", 1, 7);
    }

    @Test(expected = CantRateNotTastedBeerException.class)
    public void rate_should_throw_exception_when_beer_is_not_tasted() throws UserDoesntExistException, RatingRangeException, BeerDoesntExistException, CantRateNotTastedBeerException {
        Mockito.when(repositoryMock.getByBeerName("beer")).thenReturn(beer);
        Mockito.when(userRepository.getById(1)).thenReturn(user);
        service.rate("beer", 1, 3);
    }

    @Test
    public void filterBeers_should_return_emty_list_when_beers_doesnt_have_this_tag()
    {
        Mockito.when(repositoryMock.getAll()).thenReturn(Arrays.asList(
                new Beer(1, "beer", "descr", picture, beerDetails, user, false, 1),
                new Beer(2, "beer2", "descr", picture, beerDetails, user, false, 1),
                new Beer(3, "beer3", "descr", picture, beerDetails, user, false, 1)
        ));
        service.filterBeers("tag(TAG)");
       Assert.assertTrue(service.filterBeers("tag(TAG)").isEmpty());
    }
    @Test
    public void filterBeers_should_return_empty_list_when_beers_not_from_this_country()
    {
        service.filterBeers("Bulgaria(COUNTRY)");
        Assert.assertTrue(service.filterBeers("Bulgaria(COUNTRY)").isEmpty());
    }

}
