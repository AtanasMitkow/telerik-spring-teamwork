package com.example.beer_project.mockobjects;


import com.example.beer_project.DTO.RegistrationDTO;
import com.example.beer_project.DTO.UserDTO;
import com.example.beer_project.exceptions.*;
import com.example.beer_project.models.Beer;
import com.example.beer_project.models.BeerDetails;
import com.example.beer_project.models.User;
import com.example.beer_project.models.UserDetails;
import com.example.beer_project.models.utility.Country;
import com.example.beer_project.models.utility.Picture;
import com.example.beer_project.models.utility.Style;
import com.example.beer_project.repositories.BeerRepository;
import com.example.beer_project.repositories.UserRepository;
import com.example.beer_project.services.BeerService;
import com.example.beer_project.services.UserServiceImpl;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.*;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;

import static org.mockito.Mockito.*;
import static org.mockito.internal.verification.VerificationModeFactory.times;

@RunWith(MockitoJUnitRunner.class)
public class UserServiceImplTests {

    @Mock
    private UserRepository userRepository;

    @Mock
    private BeerRepository beerRepository;

    private User user;
    private Country country;
    private UserDetails userDetails;
    private Picture picture;
    private UserDTO userDTO;
    private RegistrationDTO registrationDTO;
    private Beer beer;
    private BeerDetails beerDetails;
    private Style style;

    @Spy
    private BeerService beerService;

    @Spy
    @InjectMocks
    private UserServiceImpl service;

    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);
        this.registrationDTO = new RegistrationDTO("username1", "password11", "user11@gmail.com", "pesho", "petrov", "picture", "Bulgaria");
        this.userDTO = new UserDTO(1, "username1", "password1", "user11@gmail.com", "ROLE_USER", false, "pesho", "petrov", "picture", "Bulgaria");
        this.picture = new Picture(1, "picture");
        this.style = new Style(1, "Svetlo");
        this.country = new Country(1, "Bulgaria");
        this.beerDetails = new BeerDetails(1, 2, "brewery", country, style);
        this.userDetails = new UserDetails("pesho", "petrov", picture, country);
        this.beer = new Beer(1, "beer", "descr", picture, beerDetails, user, false, 1);
        this.user = new User("username1", "password1", "user11@gmail.com", "ROLE_USER", true, userDetails);
    }

    @Test
    public void getByID_Should_Return_User_When_User_With_Same_ID_Exists() throws UserDoesntExistException {
        Mockito.when(userRepository.getById(1)).thenReturn(user);
        service.getById(1);
        Assert.assertEquals(user, service.getById(1));
    }

    @Test(expected = IllegalArgumentException.class)
    public void getByID_Should_Return_Null_When_USer_With_ID_Doesnt_Exists() throws UserDoesntExistException {
        Mockito.when(userRepository.getById(2)).thenThrow(new IllegalArgumentException());
        service.getById(2);
        Assert.assertNull(service.getById(2));
    }

    @Test
    public void create_Should_Create_New_User() throws UsernameAlreadyInUseException {
        Mockito.when(userRepository.getByUserName("username1")).thenReturn(null);
        service.create(user);
        verify(userRepository, Mockito.times(1)).create(user);
    }

    @Test(expected = UsernameAlreadyInUseException.class)
    public void create_Should_Throw_Exception_When_User_Already_Exists() throws UsernameAlreadyInUseException {
        Mockito.when(userRepository.getByUserName("username1")).thenReturn(user);
        service.create(user);
        verify(userRepository, Mockito.times(0)).create(user);
    }


    @Test
    public void getAll_Should_Return_Empty_List_When_There_Is_No_Users() {

        Mockito.when(userRepository.getAll()).thenReturn(new ArrayList<>());
        service.getAll();
        Assert.assertTrue(userRepository.getAll().isEmpty());
    }

    @Test
    public void getAll_Should_Return_List_When_Have_Users() {
        Mockito.when(userRepository.getAll()).thenReturn(Arrays.asList(
                new User(),
                new User(),
                new User()
        ));
        service.getAll();
        Assert.assertEquals(3, userRepository.getAll().size());
    }

    @Test
    public void GetByUserName_Should_Return_User_When_Exists() throws UserDoesntExistException {
        user.setUsername("username2");
        Mockito.when(userRepository.getByUserName("username2")).thenReturn(user);
        service.getByUserName("username2");
        Assert.assertEquals(user.getUsername(), service.getByUserName("username2").getUsername());
    }

    @Test(expected = UserDoesntExistException.class)
    public void GetByUserName_Should_Throw_Exception_When_User_Doesnt_Exists() throws UserDoesntExistException {
        Mockito.when(userRepository.getByUserName("username1")).thenReturn(null);
        service.getByUserName("username1");
    }

    @Test(expected = UserDoesntExistException.class)
    public void update_should_throw_exception_when_user_doesnt_exist() throws UserDoesntExistException {
        Mockito.when(userRepository.getById(1)).thenReturn(null);
        service.update(1, user);
    }

    @Test
    public void update_should_update_when_user__exist() throws UserDoesntExistException {
        Mockito.when(userRepository.getById(1)).thenReturn(user);
        user.setId(1);
        service.update(1, user);
        verify(userRepository, times(1)).update(1, user);
    }


    @Test(expected = UserDoesntExistException.class)
    public void delete_should_throw_exception_when_user_doesnt_exist() throws UserDoesntExistException {
        Mockito.when(userRepository.getById(1)).thenReturn(null);
        service.delete(1);
    }

    @Test
    public void delete_should_delete_when_user_exist() throws UserDoesntExistException {
        Mockito.when(userRepository.getById(1)).thenReturn(user);
        service.delete(1);
        verify(userRepository, times(1)).delete(user);

    }

    @Test(expected = UserDoesntExistException.class)
    public void getProfile_should_throw_when_user_doesnt_exist() throws UserDoesntExistException {
        Mockito.when(userRepository.getByUserName("user")).thenReturn(null);
        service.getProfile("user");
    }

    @Test
    public void getProfile_should_return_user() throws UserDoesntExistException {
        Mockito.when(userRepository.getByUserName("username1")).thenReturn(user);
        service.getProfile("username1");
        verify(service, times(1)).getProfile("username1");
    }

    @Test
    public void getProfiles_should_return_list() {
        Mockito.when(userRepository.getAll()).thenReturn(Arrays.asList(
                new User("user2", "password", "user21@gmail.com", "ROLE_USER", true, userDetails),
                new User("user3", "password", "user31@gmail.com", "ROLE_USER", true, userDetails)
        ));
        service.getProfiles();
        Assert.assertEquals(2, service.getProfiles().size());
    }

    @Test(expected = UserDoesntExistException.class)
    public void updateProfile_should_throw_exception_when_user_doesnt_exist() throws UserDoesntExistException, PasswordOutOfRangeException {
        Mockito.when(userRepository.getById(1)).thenReturn(null);
        service.updateProfile(1, new RegistrationDTO());
    }
    @Test(expected = PasswordOutOfRangeException.class)
    public void updateProfile_should_throw_exception_when_password_is_small() throws UserDoesntExistException, PasswordOutOfRangeException {
        Mockito.when(userRepository.getById(1)).thenReturn(user);
        registrationDTO.setPassword("12345");
        service.updateProfile(1, registrationDTO);
    }

    // @Test
//    public void updateProfile_should_update_when_user_exist() throws UserDoesntExistException, PasswordOutOfRangeException {
//       Helper helper = mock(Helper.class);
//        Mockito.when(userRepository.getById(1)).thenReturn(user);
//
//        doReturn(true).when(helper).;
//        service.updateProfile(1, registrationDTO);
//        verify(userRepository, times(1)).update(1, user);
//    }

    @Test(expected = BeerDoesntExistException.class)
    public void addBeerToWishlist_should_throw_exception_when_beer_doesnt_exist() throws UserDoesntExistException, BeerAlreadyInWishlistException, BeerDoesntExistException {
        Mockito.when(userRepository.getById(1)).thenReturn(user);
        Mockito.when(beerRepository.getByBeerName("beer")).thenReturn(null);
        service.addBeerToWishlist(1, "beer");
    }

    @Test(expected = UserDoesntExistException.class)
    public void addBeerToWishlist_should_throw_exception_when_user_doesnt_exist() throws UserDoesntExistException, BeerAlreadyInWishlistException, BeerDoesntExistException {
        Mockito.when(userRepository.getById(1)).thenReturn(null);
        service.addBeerToWishlist(1, "beer");
    }

    @Test
    public void addBeerToWishlist_should_add_beer() throws UserDoesntExistException, BeerAlreadyInWishlistException, BeerDoesntExistException {
        Mockito.when(userRepository.getById(1)).thenReturn(user);
        Mockito.when(beerRepository.getByBeerName("beer")).thenReturn(beer);
        Beer beer2 = new Beer(2, "beer2", "descr", picture, beerDetails, user, false, 1);
        user.addWishedBeer(beer2);
        service.addBeerToWishlist(1, "beer");
        verify(userRepository, times(1)).update(1, user);
    }

    @Test(expected = BeerDoesntExistException.class)
    public void addBeerToDranklist_should_throw_exception_when_beer_doesnt_exist() throws UserDoesntExistException, BeerDoesntExistException, BeerAlreadyInWishlistException {
        Mockito.when(userRepository.getById(1)).thenReturn(user);
        Mockito.when(beerRepository.getByBeerName("beer")).thenReturn(null);
        service.addBeerToDrinklist(1, "beer");
    }

    @Test(expected = UserDoesntExistException.class)
    public void addBeerToDranklist_should_throw_exception_when_user_doesnt_exist() throws UserDoesntExistException, BeerDoesntExistException, BeerAlreadyInWishlistException {
        Mockito.when(userRepository.getById(1)).thenReturn(null);
        service.addBeerToDrinklist(1, "beer");
    }

    @Test
    public void addBeerToDranklist_should_add_beer() throws UserDoesntExistException, BeerDoesntExistException, CantRateNotTastedBeerException, RatingRangeException, BeerAlreadyInWishlistException {
        Mockito.when(userRepository.getById(1)).thenReturn(user);
        Mockito.when(beerRepository.getByBeerName("beer")).thenReturn(beer);
        Beer beer2 = new Beer(2, "beer2", "descr", picture, beerDetails, user, false, 1);
        user.getDrankBeers().add(beer2);
        service.addBeerToDrinklist(1, "beer");
        verify(userRepository, times(1)).update(1, user);
    }

    @Test(expected = UserDoesntExistException.class)
    public void getDranklist_should_throw_exception_when_user_doesnt_exist() throws UserDoesntExistException {
        Mockito.when(userRepository.getById(1)).thenReturn(null);
        service.getDranklist(1);
    }

    @Test
    public void getDranklist_should_return_empty_list() throws UserDoesntExistException {
        Mockito.when(userRepository.getById(1)).thenReturn(user);
        Mockito.when(userRepository.getDranklist(user)).thenReturn(new HashSet<>());
        service.getDranklist(1);
        Assert.assertTrue(service.getDranklist(1).isEmpty());
    }

    @Test
    public void getDranklist_should_return_list() throws UserDoesntExistException {
        Mockito.when(userRepository.getById(1)).thenReturn(user);
        user.getDrankBeers().add(beer);
        Mockito.when(userRepository.getDranklist(user)).thenReturn(user.getDrankBeers());
        service.getDranklist(1);
        Assert.assertEquals(1, service.getDranklist(1).size());
    }

    @Test(expected = UserDoesntExistException.class)
    public void getWishlist_should_throw_exception_when_user_doesnt_exist() throws UserDoesntExistException {
        Mockito.when(userRepository.getById(1)).thenReturn(null);
        service.getWishlist(1);
    }

    @Test
    public void getWishlist_should_return_empty_list() throws UserDoesntExistException {
        Mockito.when(userRepository.getById(1)).thenReturn(user);
        Mockito.when(userRepository.getWishlist(user)).thenReturn(new HashSet<>());
        service.getWishlist(1);
        Assert.assertTrue(service.getWishlist(1).isEmpty());
    }

    @Test
    public void getWishlist_should_return_list() throws UserDoesntExistException {
        Mockito.when(userRepository.getById(1)).thenReturn(user);
        user.addWishedBeer(beer);
        Mockito.when(userRepository.getWishlist(user)).thenReturn(user.getWishlist());
        service.getWishlist(1);
        Assert.assertEquals(1, service.getWishlist(1).size());
    }

    @Test(expected = UserDoesntExistException.class)
    public void top3Beers_should_throw_exception_when_user_doesnt_exist() throws UserDoesntExistException {
        Mockito.when(userRepository.getById(1)).thenReturn(null);
        service.top3Beers(1);
    }

    @Test
    public void top3Beers_should_return_empty_list() throws UserDoesntExistException {
        Mockito.when(userRepository.getById(1)).thenReturn(user);
        Mockito.when(userRepository.top3Beers(1)).thenReturn(new ArrayList<>());
        service.top3Beers(1);
        Assert.assertTrue(service.top3Beers(1).isEmpty());
    }

    @Test(expected = IllegalArgumentException.class)
    public void registerUser_should_throw_exception_when_username_is_out_of_range() throws UsernameAlreadyInUseException, EmailAlreadyInUseException, PasswordOutOfRangeException {
        registrationDTO.setUsername("user");
        service.registerUser(registrationDTO);
    }

    @Test(expected = EmailAlreadyInUseException.class)
    public void registerUser_should_throw_exception_when_email_used() throws UsernameAlreadyInUseException, EmailAlreadyInUseException, PasswordOutOfRangeException {
        Mockito.when(userRepository.getByUserName("username1")).thenReturn(null);
        Mockito.when(userRepository.getByEmail("user11@gmail.com")).thenReturn(user);
        service.registerUser(registrationDTO);
    }

    @Test(expected = PasswordOutOfRangeException.class)
    public void register_should_throw_exception_when_password_is_small() throws PasswordOutOfRangeException, EmailAlreadyInUseException, UsernameAlreadyInUseException {
        Mockito.when(userRepository.getByUserName("username1")).thenReturn(null);
        Mockito.when(userRepository.getByEmail("user11@gmail.com")).thenReturn(null);
        registrationDTO.setPassword("12345");
        service.registerUser(registrationDTO);
    }

    @Test
    public void listUsers_Should_Return_Users() {
        Mockito.when(userRepository.getMaxPages()).thenReturn((long) 8);
        Mockito.when(userRepository.getPage(1)).thenReturn(Arrays.asList(
                new User("user2", "password", "user21@gmail.com", "ROLE_USER", true, userDetails),
                new User("user3", "password", "user31@gmail.com", "ROLE_USER", true, userDetails)
        ));

        service.listUsers(1);
        Assert.assertEquals(2, service.listUsers(1).size());
    }

    @Test(expected = IllegalArgumentException.class)
    public void listUsers_Should_Throw_Exception_When_Page_Is_Invalid() {
        service.listUsers(3);
    }

    @Test
    public void listUsers_should_return_empty_list() {
        Mockito.when(userRepository.getMaxPages()).thenReturn((long) 8);
        Mockito.when(userRepository.getPage(1)).thenReturn(new ArrayList<>());
        service.listUsers(1);
        Assert.assertTrue(service.listUsers(1).isEmpty());
    }

    @Test
    public void getMaxPages_Should_return() {
        Mockito.when(userRepository.getMaxPages()).thenReturn((long) 7);
        service.getMaxPages();
        Assert.assertTrue(3 > service.getMaxPages());
    }

}
