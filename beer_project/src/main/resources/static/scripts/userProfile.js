
function showCurrentUserProfile() {
    activateProfile();
    let container = $("#content");
    $.ajax({
        url: `http://localhost:8080/me`,
        success: function (response) {
            let id = response.id;
            let username = response.username;
            $.ajax({
                url: `http://localhost:8080/api/profiles/${username}`,
                success: function (response) {
                    let fullName = `${response.firstName} ${response.lastName}`;
                    container.html("");
                    container.append(`<div class="top-container">
             <div class="left-column">
               <img class="user-ava" src="${response.picture}">
</div>
          
              <div class="right-column">
                <content class = "user-description">
                  <h1>${fullName}</h1>
                  <p class="country">${response.country}</p>                    
                 </content>
              <div class="buttons">
                  <button type="button" onclick="deleteProfile('${response.id}',false)" class="userbtn">Delete profile</button>
                  <button type="button" onclick="toggleUpdateUserModal('${response.id}')" class="userbtn">Edit profile</button>
                  <button type="button" onclick="getUserList('${response.id}','${dranklist}')" class="userbtn">View drank beers</button>
                  <button type="button" onclick="getUserList('${response.id}','${wishlist}')" class="userbtn">View wishlist</button>
                
                     </div>
                    
                </div>
               
              </div>
 <div class="bottom-container"></div>`);

                    let bottomContainer = $(".bottom-container");
                    showTop3Beers(id, bottomContainer);

                }
            })
        }
    })
}


function showUserProfile(username) {
    activateProfile();
    let container = $("#content");
    $.ajax({
        url: `http://localhost:8080/api/profiles/${username}`,
        success: function (response) {
            let fullName = `${response.firstName} ${response.lastName}`;
            container.html("");
            container.append(`<div class="top-container">
             <div class="left-column">
               <img class="user-ava" src="${response.picture}">
</div>
          
              <div class="right-column">
                <content class = "user-description">
                  <h1>${fullName}</h1>
                  <p class="country">${response.country}</p>                    
                 </content>
              <div class="buttons">
                  <button type="button" onclick="deleteProfile('${response.id}',true)" class="userbtn">Delete profile</button>
                  <button type="button" onclick="getUserList('${response.id}','${dranklist}')" class="userbtn">View drank beers</button>
                            <button type="button" onclick="getUserList('${response.id}','${wishlist}')" class="userbtn">View wishlist</button>
                            </div>
                    
                </div>
               
              </div>
 <div class="bottom-container"></div>`);

            let bottomContainer = $(".bottom-container");
            showTop3Beers(response.id, bottomContainer);

        }
    })
}

function showTop3Beers(user, container) {
    $.ajax({
        url: `http://localhost:8080/api/users/${user}/top3`,
        success: function (response) {
            $.each(response, function (i, rowSize) {
                container.append(`<div class="beer-wrapper important-margin-bottom fade-in">
                      <div class="profile">
                        <img src="${response[i].picture}" class="thumbnail">
                        <h3 class="name">${response[i].name}</h3>
                        <p class="title">${response[i].creator}</p>
                        <p class="beer-description">${response[i].description}</p>
                      </div>
                      <button type="button" class="btn" onclick="showBeerProfile('${response[i].name}')">View more</button>   
                    </div>`)
            })
        }
    })
}

let currentTypeOfList = 'wishlist';
let listCurrentPage = 1;
const wishlist = 'wishlist';
const dranklist = 'dranklist';

function getUserList(userid, list) {
    if(list !== currentTypeOfList)
    {
        listCurrentPage = 1;
        currentTypeOfList = list;
    }
    $.ajax({
        url: `http://localhost:8080/api/users/${userid}/${list}`,
        success: function (response) {
            showFilteredBeers(listCurrentPage, Math.floor(response.length / 8) + 1, response);
            const leftArrow = $("#leftArrow");
            const rightArrow = $("#rightArrow");
            leftArrow.attr("onclick", `showPrevList('${userid}','${list}')`);
            rightArrow.attr("onclick", `showNextList('${userid}','${list}')`);
        }
    })
}


function showPrevList(userid, list) {
    listCurrentPage--;
    getUserList(userid, list)
}

function showNextList(userid, list) {
    listCurrentPage++;
    getUserList(userid, list);
}

function deleteProfile(userid,isAdmin) {
    $.ajax({
        url: `http://localhost:8080/api/users/${userid}`,
        type: 'DELETE',
        beforeSend: function (xhr) {
            xhr.overrideMimeType("text/plain; charset=x-user-defined");
        },
        success: function () {
            if(isAdmin === false) {
                let logoutForm = $(".logout");
                logoutForm.trigger("submit");
            }
            else displayUsers();
            snackbarToggle("Profile deleted", "green");
        },
        error: function()
        {
            snackbarToggle("Couldn't delete profile","red");
        }
    })
}