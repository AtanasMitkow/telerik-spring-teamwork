function redirectHome() {
    window.location.replace("http://localhost:8080/");
}

function redirectLogin() {
    window.location.replace("http://localhost:8080/login");
}

function activateUsers()
{
    let buttonToActivate = $("#usersButton");
    $('ul li .active').removeClass('active');
    buttonToActivate.addClass('active');
}

function activateProfile() {
    let buttonToActivate = $("#profileButton");
    $('ul li .active').removeClass('active');
    buttonToActivate.addClass('active');
}


