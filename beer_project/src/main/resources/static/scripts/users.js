let userCurrentPage = 1;
function nextUserPage()
{
    userCurrentPage++;
    displayUsers();
}

function prevUserPage()
{
    userCurrentPage--;
    displayUsers();
}


function showUsers(page,maxPages) {
    insertGrid();
    activateUsers();
    userCurrentPage = page;
    const leftArrow = $("#leftArrow");
    const rightArrow = $("#rightArrow");
    leftArrow.attr("onclick","prevUserPage()");
    rightArrow.attr("onclick","nextUserPage()");
    if (page === 1) leftArrow.css("display", "none");
    else leftArrow.css("display", "block");
    if (page === maxPages) rightArrow.css("display", "none");
    else rightArrow.css("display", "block");

    let row1 = $('#row1');
    let row2 = $('#row2');
    row1.html(" ");
    row2.html(" ");
    let currentRow = row1;
    $.ajax({
        url: `http://localhost:8080/api/profiles?page=${page}`,
        success: function (response) {
            $.each(response, function (i, rowSize) {
                if (i === 4) currentRow = row2;
                if (i === 8) return false;
                let fullName = `${response[i].firstName} ${response[i].lastName}`;
                currentRow.append(`<div class="column bclmn">
        <div class="beer-wrapper">
            <div class="profile">
                <img src="${response[i].picture}" class="thumbnail">
                <h3 class="name">${response[i].username}</h3>
                <p class="title">${response[i].email}</p>
                <p class="beer-description">${fullName}</p>
            </div>
            <button type="button" onclick="showUserProfile('${response[i].username}')" class="btn">View more</button>
        </div>
    </div>`)
            });
        }
    })
}

function displayUsers()
{
    $.ajax({
        url: "http://localhost:8080/api/users/pages",
        success: function (response) {
            showUsers(userCurrentPage,response);
        }
    })
}
