// Get the modal
let createModal = $('#createModal');

// Get the button that opens the modal
let btn = $("#newbeer");

// Get the <span> element that closes the modal
let span = $("#close");

let updateModal = $("#updateModal");

let updatespan = $("#updateClose");

let addTagModal = $("#addTagModal");

let addTagSpan = $("#addTagClose");

let updateUserModal = $("#updateUserModal");

let updateUserClose = $("#updateUserClose");

// When the user clicks on the button, open the modal
btn.click(function () {
    createModal.css("display", "block");
});

// When the user clicks on <span> (x), close the modal
span.click(function () {
    createModal.css("display", "none");
});

function toggleUpdateModal() {
    updateModal.css("display", "block");
}

let currentUserId = 0;

function toggleUpdateUserModal(userid)
{
    updateUserModal.css("display","block");
    currentUserId=userid;
}

updatespan.click(function () {
    updateModal.css("display", "none");
});

addTagSpan.click(function () {
    addTagModal.css("display", "none")
});

updateUserClose.click(function ()
{
    updateUserModal.css("display","none");
});

function toggleAddTagModal() {
    addTagModal.css("display", "block");
}

function snackbarToggle(text, color) {
    let x = $("#snackbar");
    x.css("background-color", color);
    x.text(text);

    x.addClass("show");

    // After 3 seconds, remove the show class from DIV
    setTimeout(function () {
        x.removeClass("show");
    }, 3000);
}

//Create

function create() {
    let name = $('#name').val();
    let desc = $('#desc').val();
    let abv = $('#abv').val();
    let brew = $('#brew').val();
    let country = $('#country option:selected').val();
    let style = $('#style option:selected').val();
    let pics = $('#pic').val();
    let tags = $('#tags').val();
    $.get({
        url: "http://localhost:8080/me",
        success: function (response) {
            if (response.id === undefined) {
                redirectLogin();
            } else {
            var body = {
                name: name,
                description: desc,
                tag: tags,
                abv: abv,
                picture: pics,
                creator: response.username,
                brewery: brew,
                country: country,
                style: style
            };
            $.ajax({
                url: 'http://localhost:8080/api/beers/new',
                type: 'POST',
                contentType: 'application/json',
                data: JSON.stringify(body),
                dataType: 'json',
                success: function () {
                    createModal.css("display", "none");
                    snackbarToggle("Beer created successfully", "green");
                    displayBeers(defaultCriteria);
                },
                error: function (request) {
                    let errorObj = JSON.parse(request.responseText);
                    snackbarToggle(errorObj.message, "red");
                }
            });}
        },
        error: function (request) {
            let errorObj = JSON.parse(request.responseText);
            snackbarToggle(errorObj.message, "red");
        }
    });


}


function updateBeer() {

    $.ajax({
        url: `http://localhost:8080/me`,
        success: function (response) {
            if (response.id === undefined) {
               redirectLogin();
            } else {

                let name = $('#beerName').text();
                let desc = $('#updatedesc').val();
                let abv = $('#updateabv').val();
                let brew = $('#updatebrew').val();
                let pics = $('#updatepic').val();

                let body = {
                    description: desc,
                    abv: abv,
                    picture: pics,
                    brewery: brew
                };
                $.ajax({
                    url: `http://localhost:8080/api/beers/${name}/`,
                    type: 'PUT',
                    contentType: 'application/json',
                    data: JSON.stringify(body),
                    beforeSend: function (xhr) {
                        xhr.overrideMimeType("text/plain; charset=x-user-defined");
                    },
                    success: function (response) {
                        updateModal.css("display", "none");
                        snackbarToggle("Beer updated successfully", "green");
                        showBeerProfile(name);
                    },
                    error: function (request) {
                        let errorObj = JSON.parse(request.responseText);
                        snackbarToggle(errorObj.message, "red");
                    }
                });
            }
        }
    })
}


function updateUser()
{
    let newPassword = $("#userUpdatePassword").val();
    let newFirstName = $("#userUpdateFirstname").val();
    let newLastName = $("#userUpdateLastname").val();
    let newCountry = $("#userUpdateCountry option:selected").val();
    let newPicture = $("#userUpdatePicture").val();

    let body =
    {
        password: newPassword,
        firstName: newFirstName,
        lastName: newLastName,
        countryName: newCountry,
        pictureUrl: newPicture
    };

    $.ajax({
        url: `http://localhost:8080/api/profiles/edit/${currentUserId}`,
        type: 'PUT',
        beforeSend: function (xhr) {
            xhr.overrideMimeType("text/plain; charset=x-user-defined");
        },
        contentType: 'application/json',
        data: JSON.stringify(body),
        success: function (response) {
            updateUserModal.css("display", "none");
            snackbarToggle("Profile updated successfully", "green");
            showCurrentUserProfile();
        },
        error: function (request) {
            let errorObj = JSON.parse(request.responseText);
            snackbarToggle(errorObj.message, "red");
        }
    })
}

function addTag() {
    $.ajax({
        url: `http://localhost:8080/me`,
        success: function (response) {
            if (response.id === undefined) {
                redirectLogin();
            } else {
                let beerName = $('#beerName').text();
                let tag = $('#tag').val();

                $.ajax({
                    url: `http://localhost:8080/api/beers/${beerName}/addTag?tagName=${tag}`,
                    type: 'PUT',
                    beforeSend: function (xhr) {
                        xhr.overrideMimeType("text/plain; charset=x-user-defined");
                    },
                    success: function (response) {
                        addTagModal.css("display", "none");
                        snackbarToggle("Tag added successfully", "green");
                        showBeerProfile(beerName);
                    },
                    error: function (request) {
                        let errorObj = JSON.parse(request.responseText);
                        snackbarToggle(errorObj.message, "red");
                    }
                })
            }
        }

    })
}

