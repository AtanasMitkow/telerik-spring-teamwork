function addToWishList(beername) {
    $.ajax({
        url: `http://localhost:8080/me`,
        success: function (response) {
            if (response.id === undefined) {
                redirectLogin();
            } else {
                let beerToAdd = $("#beerName").text();
                $.ajax({
                    url: `http://localhost:8080/api/users/${response.id}/wishBeer/${beerToAdd}/`,
                    type: 'PUT',
                    beforeSend: function (xhr) {
                        xhr.overrideMimeType("text/plain; charset=x-user-defined");
                    },
                    success: function () {
                        snackbarToggle("Beer added to wishlist!", "green");
                    },
                    error: function (request) {
                        let errorObj = JSON.parse(request.responseText);
                        snackbarToggle(errorObj.message, "red");
                    }
                })
            }
        }
    })
}

function addToDrankList(beername) {
    $.ajax(
        {
            url: `http://localhost:8080/me`,
            success: function (response) {
                if (response.id === undefined) {
                    redirectLogin();
                } else {
                    let beerToAdd = $("#beerName").text();
                    $.ajax({
                        url: `http://localhost:8080/api/users/${response.id}/drinkBeer/${beerToAdd}/`,
                        type: 'PUT',
                        beforeSend: function (xhr) {
                            xhr.overrideMimeType("text/plain; charset=x-user-defined");
                        },
                        success: function () {
                            snackbarToggle("Cheers!", "green");
                        },
                        error: function (request) {
                            let errorObj = JSON.parse(request.responseText);
                            snackbarToggle(errorObj.message, "red");
                        }
                    })
                }
            }
        }
    )
}

function showBeerProfile(name) {
    activateBeers();
    let container = $("#content");
    $.ajax({
        url: `http://localhost:8080/api/beers?name=${name}`,
        success: function (response) {
            let stars = fillStars(response.rating);
            container.html("");
            container.append(` <div class="row">
              
              <div class="column small">
                  <img class="img-ava" src="${response.picture}">
                    <button type="button" class="det" onclick="addToWishList('${response.name}')">Add to wishlist</button>
                    <button type="button" class="det" onclick="addToDrankList('${response.name}')">Mark as drank</button>
                    <button type="button" class="det" onclick="toggleUpdateModal()" >Edit beer</button>
                </div>
                <div class="column big">
                    <content class = "description">
                        
                        <h1 id="beerName">${response.name}</h1>
                        <div class="wrapper">
                          <input type="checkbox" id="st1"  value="1" />
                          <label onclick="rateBeer('${response.name}',5)" for="st1"></label>
                          <input type="checkbox" id="st2" value="2" />
                          <label onclick="rateBeer('${response.name}',4)" for="st2"></label>
                          <input type="checkbox" id="st3" value="3" />
                          <label onclick="rateBeer('${response.name}',3)" for="st3"></label>
                          <input type="checkbox" id="st4" value="4" />
                          <label onclick="rateBeer('${response.name}',2)" for="st4"></label>
                          <input type="checkbox" id="st5" value="5" />
                          <label onclick="rateBeer('${response.name}',1)" for="st5"></label>
                        </div>
                        <p><b>Rating:&nbsp</b><span>${stars}</span></p>
                        <p><b>ABV:&nbsp</b>${response.abv}\%</p>
                        <p><b>Brewery:&nbsp </b>${response.brewery}</p>
                        <p><b>Country:&nbsp</b>${response.country}</p>
                        <p><b>Style:&nbsp</b>${response.style}</p>
                        <p><b>Description:</b><br>${response.description}</p>
                        <p><button class="plus-button" onclick="toggleAddTagModal()">+</button><b>Tags:&nbsp</b>${response.tag}</p>
                        <p><b>Creator:&nbsp</b>${response.creator}</p>
                      </content>
                </div>
                  
              </div>
            </div>`
            );
        }
    })
}

function rateBeer(beerName,rating)
{
    $.ajax(
        {
            url: `http://localhost:8080/me`,
            success: function (response) {
                if (response.id === undefined) {
                    redirectLogin();
                } else {
                    $.ajax({
                        url: `http://localhost:8080/api/beers/${beerName}/rate/${response.id}/${rating}`,
                        type: 'PUT',
                        beforeSend: function (xhr) {
                            xhr.overrideMimeType("text/plain; charset=x-user-defined");
                        },
                        success: function () {
                            snackbarToggle("Cheers!", "green");
                            showBeerProfile(beerName);
                        },
                        error: function (request) {
                            let errorObj = JSON.parse(request.responseText);
                            snackbarToggle(errorObj.message, "red");
                        }
                    })
                }
            }
        }
    )
}
