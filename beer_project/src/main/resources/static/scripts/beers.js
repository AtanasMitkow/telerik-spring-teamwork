let currentPage = 1;



function activateBeers()
{
    let buttonToActivate = $("#beersButton");
    $('ul li .active').removeClass('active');
    buttonToActivate.addClass('active');
}

function nextPage() {
    currentPage++;
    displayBeers(defaultCriteria)
}

function prevPage() {
    currentPage--;
    displayBeers(defaultCriteria);
}

function insertGrid() {
    let content = $("#content");
    content.html("");
    content.append(`<div class="middle-left"><i id="leftArrow" class="arrow left" onclick="prevPage()"></i></div>
<div class="row" id="row1" >

</div>
<div class="row" id="row2">

</div>

<div class="middle-right"><i class="arrow right" id="rightArrow" onclick="nextPage()"></i></div>`)
}

function loadBeers(page,maxPage,criteria,order) {
    insertGrid();
    activateBeers();
    currentPage = page;
    const leftArrow = $("#leftArrow");
    const rightArrow = $("#rightArrow");
    if (page === 1) leftArrow.css("display", "none");
    else leftArrow.css("display", "block");
    if (page === maxPage) rightArrow.css("display", "none");
    else rightArrow.css("display", "block");

    let row1 = $('#row1');
    let row2 = $('#row2');
    row1.html(" ");
    row2.html(" ");
    let currentRow = row1;
    $.ajax({
        url: `http://localhost:8080/api/beers/profiles?page=${page}&crit=${criteria}&order=${order}`,
        success: function (response) {
            $.each(response, function (i, rowSize) {
                let stars= fillStars(response[i].rating);
                if (i === 4) currentRow = row2;
                if (i === 8) return false;
                currentRow.append(`<div class="column bclmn">
        <div class="beer-wrapper fade-in">
            <div class="profile">
                <img src="${response[i].picture}" class="thumbnail">
                <h3 class="name">${response[i].name}</h3>
                <p class="title">${response[i].creator}</p>
                <span class = "centered" id="stars">${stars}</span>
                <p class="beer-description">${response[i].description}</p>
            </div>
            <button type="button" onclick="showBeerProfile('${response[i].name}')" class="btn">View more</button>
        </div>
    </div>`)
            });
        },
        error: function (request) {
            let errorObj = JSON.parse(request.responseText);
            snackbarToggle(errorObj.message, "red");
        }
    })

}

function fillStars(rating)
{
    rating = Math.round(rating*2)/2;
    let stars = [];
    for (var i = rating; i >= 1; i--)
        stars.push('<i class="fa fa-star" aria-hidden="true" style="color: gold;"></i>&nbsp;');

    // If there is a half a star, append it
    if (i === .5) stars.push('<i class="fa fa-star-half-o" aria-hidden="true" style="color: gold;"></i>&nbsp;');

    // Fill the empty stars
    for (let i = (5 - rating); i >= 1; i--)
        stars.push('<i class="fa fa-star-o" aria-hidden="true" style="color: gold;"></i>&nbsp;');
    return stars.join("");
}

function displayBeers(criteria)
{
    if(criteria===undefined) criteria="";
    if(!order.includes("(")) order="name(asc)";
    $.ajax({
        url: `http://localhost:8080/api/beers/pages?crit=${criteria}`,
        success: function (response) {
            loadBeers(currentPage,response,criteria,order);
        },
        error: function (request) {
            let errorObj = JSON.parse(request.responseText);
            snackbarToggle(errorObj.message, "red");
        }
    })
}


let defaultCriteria = "";
let order = $("#sortBy option:selected").val();
displayBeers(defaultCriteria);


