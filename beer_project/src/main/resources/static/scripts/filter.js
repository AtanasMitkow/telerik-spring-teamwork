const pageSize = 8;

let filteredCurrentPage = 1;
let filterCriteria;
function filter(criteria) {
    filterCriteria = criteria;
    $.ajax({
        url: `http://localhost:8080/api/beers/filter?filter=${criteria}`,
        success: function (response) {
            showFilteredBeers(filteredCurrentPage,Math.floor((response.length/pageSize))+1,response);
        },
        error: function (request) {
            let errorObj = JSON.parse(request.responseText);
            snackbarToggle(errorObj.message, "red");
        }
    })
}

function showNextFiltered()
{
    filteredCurrentPage++;
    filter(filterCriteria);
}

function showPrevFiltered() {
    filteredCurrentPage--;
    filter(filterCriteria);
}

function showFilteredBeers(page, maxPage, response) {
    insertGrid();
    activateBeers();

    let sliceBegin = (page-1)*8;
    let sliceEnd = sliceBegin+8;
    response = response.slice(sliceBegin,sliceEnd);

    const leftArrow = $("#leftArrow");
    const rightArrow = $("#rightArrow");
    leftArrow.attr("onclick","showPrevFiltered()");
    rightArrow.attr("onclick","showNextFiltered()");

    if (page === 1) leftArrow.css("display", "none");
    else leftArrow.css("display", "block");
    if (page === maxPage) rightArrow.css("display", "none");
    else rightArrow.css("display", "block");

    let row1 = $('#row1');
    let row2 = $('#row2');
    row1.html(" ");
    row2.html(" ");
    let currentRow = row1;

    $.each(response, function (i, rowSize) {
        if (i === 4) currentRow = row2;
        if (i === 8) return false;
        currentRow.append(`<div class="column bclmn">
        <div class="beer-wrapper fade-in">
            <div class="profile">
                <img src="${response[i].picture}" class="thumbnail">
                <h3 class="name">${response[i].name}</h3>
                <p class="title">${response[i].creator}</p>
                <p class="beer-description">${response[i].description}</p>
            </div>
            <button type="button" onclick="showBeerProfile('${response[i].name}')" class="btn">View more</button>
        </div>
    </div>`);
    })
}