function searchByCriteria()
{
    let sortBy = $("#sortBy option:selected").val();
    let searchCriteria = $("#searchCriteria").val();

    defaultCriteria = searchCriteria;
    order = sortBy;
    currentPage = 1;
    filteredCurrentPage = 1;
    if(sortBy.includes('(')) {
        displayBeers(defaultCriteria);
    }
    else
    {
        let composedFilter = `${searchCriteria}(${order})`;
        filter(composedFilter);
    }
}