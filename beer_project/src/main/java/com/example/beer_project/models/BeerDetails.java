package com.example.beer_project.models;


import com.example.beer_project.models.utility.Country;
import com.example.beer_project.models.utility.Style;

import javax.persistence.*;
import javax.validation.constraints.Size;

@Entity
@Table(name = "beer_details")
public class BeerDetails {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "beer_details_id", nullable = false, updatable = false)
    private long id;

    @Column(name = "abv")
    private int abv;

    @Size(min=2,message = "Brewery with name less than 2 symbols does not exist")
    @Column(name = "brewery", nullable = false)
    private String brewery;

    @OneToOne
    @JoinColumn(name = "country_id")
    private Country country;

    @OneToOne
    @JoinColumn(name = "style_id")
    private Style style;

    public BeerDetails() {
    }

    public BeerDetails(long id, int abv, String brewery, Country country, Style style) {
        this.id = id;
        this.abv = abv;
        this.brewery = brewery;
        this.country = country;
        this.style = style;
    }

    public long getId() {
        return id;
    }

    public int getAbv() {
        return abv;
    }

    public String getBrewery() {
        return brewery;
    }

    public Country getCountry() {
        return country;
    }

    public Style getStyle() {
        return style;
    }

    public void setId(long id) {
        this.id = id;
    }

    public void setAbv(int abv) {
        if(abv<1||abv>100) throw new IllegalArgumentException("Beer with more than 100% abv does not exist");
        this.abv = abv;
    }

    public void setBrewery(String brewery) {
        if(brewery.length()<2) throw new IllegalArgumentException("Brewery with name less than 2 symbols does not exist");
        this.brewery = brewery;
    }

    public void setCountry(Country country) {
        this.country = country;
    }

    public void setStyle(Style style) {
        this.style = style;
    }
}
