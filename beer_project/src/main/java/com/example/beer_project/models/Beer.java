package com.example.beer_project.models;

import com.example.beer_project.models.utility.Picture;
import com.example.beer_project.models.utility.Tag;
import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import javax.validation.constraints.Size;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;


@Entity
@Table(name = "beers")
public class Beer {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "beer_id", nullable = false, updatable = false)
    private long id;

    @Column(name = "name", nullable = false, unique = true)
    @Size(min = 3, max = 35, message = "Beer name must be between 3 and 30 symbols")
    private String name;

    @Column(name = "description")
    private String description;

    @ManyToMany(cascade = CascadeType.ALL)
    @JoinTable(
            name = "beer_tags",
            joinColumns = @JoinColumn(name = "beer_id"),
            inverseJoinColumns = @JoinColumn(name = "tag_id")
    )
    private Set<Tag> tags;

    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "picture_id")
    private Picture picture;

    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "beer_details_id")
    private BeerDetails beerDetails;

    @ManyToOne
    @JoinColumn(name = "user_id")
    private User creator;

    @Column(name = "is_deleted", nullable = false)
    private boolean isDeleted;

    @ManyToMany(mappedBy = "drankBeers",fetch = FetchType.EAGER)
    @JsonIgnore
    private List<User> drankByUsers = new ArrayList<>();

    @Column(name = "rating")
    private double rating;

    public Beer() {
        tags = new HashSet<>();
        rating=0;
    }

    public Beer(long id,String name, String description, Picture picture, BeerDetails beerDetails, User creator, boolean isDeleted, double rating) {
        this.id=id;
        this.name = name;
        this.description = description;
        tags = new HashSet<>();
        this.picture = picture;
        this.beerDetails = beerDetails;
        this.creator = creator;
        this.isDeleted = isDeleted;
        drankByUsers = new ArrayList<>();
        this.rating = rating;
    }

    public long getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getDescription() {
        return description;
    }

    public Set<Tag> getTag() {
        return tags;
    }

    public Picture getPicture() {
        return picture;
    }

    public BeerDetails getBeerDetails() {
        return beerDetails;
    }

    public User getCreator() {
        return creator;
    }

    public boolean isDeleted() {
        return isDeleted;
    }

    public double getRating() {
        return rating;
    }

    public void setId(long id) {
        this.id = id;
    }

    public void setName(String name) {
        if(name.length()<3 || name.length()>30) throw new IllegalArgumentException("Beer name must be between 3 and 30 symbols");
        this.name = name;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public void setTags(Set<Tag> tags) {
        this.tags = tags;
    }

    public void setPicture(Picture picture) {
        this.picture = picture;
    }

    public void setBeerDetails(BeerDetails beerDetails) {
        this.beerDetails = beerDetails;
    }

    public void setCreator(User creator) {
        this.creator = creator;
    }

    public void setDeleted(boolean deleted) {
        isDeleted = deleted;
    }

    public void setRating(double rating) {
        if(rating<1 || rating>5)throw new IllegalArgumentException("Rating must be between 1 and 5");
        this.rating = rating;
    }

}
