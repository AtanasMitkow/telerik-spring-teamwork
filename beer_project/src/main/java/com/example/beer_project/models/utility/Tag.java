package com.example.beer_project.models.utility;

import javax.persistence.*;
import java.util.Objects;

@Entity
@Table(name = "tags")
public class Tag {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "tag_id", updatable = false, nullable = false)
    private long tag_id;

    @Column(name = "name", nullable = false, unique = true)
    private String name;

    public Tag() {
    }

    public Tag(String name) {
        this.name = name;
    }

    public long getId() {
        return tag_id;
    }

    public String getName() {
        return name;
    }

    public void setId(long id) {
        this.tag_id = id;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString()
    {
        return this.name;
    }

    @Override
    public boolean equals (Object object) {
        boolean result = false;
        if (object == null || object.getClass() != getClass()) {
            result = false;
        } else {
            Tag tag = (Tag) object;
            if (this.name.equals(tag.getName())) {
                result = true;
            }
        }
        return result;
    }

    @Override
    public int hashCode()
    {
        return Objects.hash(this.name);
    }
}
