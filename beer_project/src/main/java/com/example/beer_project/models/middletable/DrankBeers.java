package com.example.beer_project.models.middletable;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name = "DrankBeers")
public class DrankBeers {
    @EmbeddedId
   private DrankBeersID id;

    @Column(name = "rating")
    private double rating = 0;

    public DrankBeers() {
    }

    public DrankBeersID getId() {
        return id;
    }

    public double getRating() {
        return rating;
    }

    public void setId(DrankBeersID id) {
        this.id = id;
    }

    public void setRating(double rating) {
        this.rating = rating;
    }
}
