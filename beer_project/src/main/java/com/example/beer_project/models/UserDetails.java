package com.example.beer_project.models;

import com.example.beer_project.models.utility.Country;
import com.example.beer_project.models.utility.Picture;

import javax.persistence.*;
import javax.validation.constraints.Size;


@Entity
@Table(name = "user_details")
public class UserDetails {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "user_details_id", nullable = false, updatable = false)
    private long id;

    @Size(min = 2,message = "First name cannot be less than 2 symbols")
    @Column(name = "firstname", nullable = false)
    private String firstName;

    @Size(min = 2, message = "Last name cannot be less than 2 symbols")
    @Column(name = "lastname")
    private String lastName;

    @OneToOne(cascade = CascadeType.ALL, orphanRemoval = true)
    @JoinColumn(name = "picture_id")
    private Picture picture;

    @OneToOne
    @JoinColumn(name = "country_id")
    private Country country;


    public UserDetails() {
    }

    public UserDetails(String firstName, String lastName, Picture picture, Country country) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.picture = picture;
        this.country = country;
    }


    public long getId() {
        return id;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public Picture getPicture() {
        return picture;
    }

    public Country getCountry() {
        return country;
    }

    public void setId(long id) {
        this.id = id;
    }

    public void setFirstName(String firstName) {
        if(firstName.length()<2) throw new IllegalArgumentException("First name cannot be less than 2 symbols");
        this.firstName = firstName;
    }

    public void setLastName(String lastName) {
        if(lastName.length()<2) throw new IllegalArgumentException("Last name cannot be less than 2 symbols");
        this.lastName = lastName;
    }

    public void setPicture(Picture picture) {
        this.picture = picture;
    }

    public void setCountry(Country country) {
        this.country = country;
    }
}
