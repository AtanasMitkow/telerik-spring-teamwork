package com.example.beer_project.models;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import javax.validation.constraints.Size;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Entity
@Table(name = "users")
public class User {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "user_id", nullable = false, updatable = false)
    private long id;

    @Column(name = "username", unique = true, nullable = false)
    @Size(min = 8, max = 28, message = "Username must be between 8 and 23 symbols!")
    private String username;

    @Column(name = "password", nullable = false)
    @Size(min = 8, max = 60, message = "Password must be between 8 and 60 symbols!")
    private String password;

    @Size(min = 15,max=50, message = "Email must be between 15 and 45 symbols!")
    @Column(name = "email", nullable = false, unique = true)
    private String email;

    @Column(name = "authority", nullable = false)
    private String role;

    @Column(name = "enabled", nullable = false)
    private boolean enabled;

    @OneToOne(cascade = CascadeType.ALL, orphanRemoval = true)
    @JoinColumn(name = "user_details_id")
    private UserDetails userDetails;

    @OneToMany(mappedBy = "creator")
    @JsonIgnore
    private List<Beer> createdBeers;

    @ManyToMany(cascade = {CascadeType.PERSIST, CascadeType.MERGE}, fetch = FetchType.EAGER)
    @JoinTable(name = "DrankBeers",
            joinColumns = @JoinColumn(name = "user_id")
            , inverseJoinColumns = @JoinColumn(name = "beer_id"))
    @JsonIgnore
    private Set<Beer> drankBeers = new HashSet<>();

    @ManyToMany(cascade = CascadeType.ALL)
    @JoinTable(
            name = "wishlist",
            joinColumns = @JoinColumn(name = "user_id"),
            inverseJoinColumns = @JoinColumn(name = "beer_id")
    )
    @JsonIgnore
    private Set<Beer> wishlist;


    public User() {
    }

    public User(String username, String password, String email, String role, boolean enabled, UserDetails userDetails) {
        this.username = username;
        this.password = password;
        this.email = email;
        this.role = role;
        this.enabled = enabled;
        this.userDetails = userDetails;
        wishlist=new HashSet<>();
        drankBeers=new HashSet<>();
    }

    public long getId() {
        return id;
    }

    public String getUsername() {
        return username;
    }

    public String getPassword() {
        return password;
    }

    public String getEmail() {
        return email;
    }

    public String getRole() {
        return role;
    }

    public boolean isEnabled() {
        return enabled;
    }

    public UserDetails getUserDetails() {
        return userDetails;
    }

    public Set<Beer> getDrankBeers() {
        return drankBeers;
    }

    public Set<Beer> getWishlist() {
        return wishlist;
    }

    public List<Beer> getCreatedBeers() {
        return createdBeers;
    }


    public void setId(long id) {
        this.id = id;
    }

    public void setUsername(String username) {
        if(username.length()<8 ||username.length()>23) throw new IllegalArgumentException("Username must be between 8 and 23 symbols!");
        this.username = username;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public void setEmail(String email) {
        if(email.length()<15||email.length()>45) throw new IllegalArgumentException("Email must be between 15 and 45 symbols!");
        this.email = email;
    }

    public void setRole(String role) {
        this.role = role;
    }

    public void setEnabled(boolean enabled) {
        this.enabled = enabled;
    }

    public void setUserDetails(UserDetails userDetails) {
        this.userDetails = userDetails;
    }

    public void setCreatedBeers(List<Beer> createdBeers) {
        this.createdBeers = createdBeers;
    }

    public void setWishlist(Set<Beer> wishlist) {
        this.wishlist = wishlist;
    }

    public void setDrankBeers(Set<Beer> drankBeers) {
        this.drankBeers = drankBeers;
    }

    public void addWishedBeer(Beer beer) {
        this.wishlist.add(beer);
    }
}
