package com.example.beer_project.models.middletable;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import java.io.Serializable;
import java.util.Objects;

@Embeddable
public class DrankBeersID implements Serializable {
    @Column(name = "user_id")
    private long userId;

    @Column(name = "beer_id")
    private long beerId;

    public DrankBeersID() {
    }

    public DrankBeersID(Integer userId, Integer beerId) {
        this.userId = userId;
        this.beerId = beerId;
    }

    public long getUserId() {
        return userId;
    }

    public long getBeerId() {
        return beerId;
    }

    public void setUserId(long userId) {
        this.userId = userId;
    }

    public void setBeerId(long beerId) {
        this.beerId = beerId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof DrankBeersID)) return false;
        DrankBeersID that = (DrankBeersID) o;
        return Objects.equals(getUserId(), that.getUserId()) &&
                Objects.equals(getBeerId(), that.getBeerId());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getUserId(), getBeerId());
    }
}
