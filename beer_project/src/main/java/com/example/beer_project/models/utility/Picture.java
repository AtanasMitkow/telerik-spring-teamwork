package com.example.beer_project.models.utility;

import javax.persistence.*;

@Entity
@Table(name = "pictures")
public class Picture {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "picture_id", nullable = false, updatable = false)
    private long id;

    @Column(name = "url")
    private String url;

    public Picture() {
    }

    public Picture(int id , String url) {
        this.id=id;
        this.url = url;
    }
    public Picture(String url) {
        this.url = url;
    }

    public long getId() {
        return id;
    }

    public String getUrl() {
        return url;
    }

    public void setId(long id) {
        this.id = id;
    }

    public void setUrl(String url) {
        this.url = url;
    }
}
