package com.example.beer_project.repositories.utility;

import com.example.beer_project.models.utility.Country;

import java.util.List;

public interface CountryRepository {
    List<Country> getAll();

    Country getByName(String name);

}
