package com.example.beer_project.repositories;

import com.example.beer_project.models.Beer;
import com.example.beer_project.models.User;

import java.util.List;
import java.util.Set;

public interface UserRepository {
    void create(User user);

    void update(long id, User user);

    List<User> getAll();

    User getByUserName(String name);

    User getById(long id);

    User getByEmail(String email);

    Set<Beer> getWishlist(User user);

    Set<Beer> getDranklist(User user);

    List<Long> top3Beers(long id);

    Long getMaxPages();

    List<User> getPage(int page);

    void delete(User user);
}
