package com.example.beer_project.repositories;

import com.example.beer_project.models.Beer;
import com.example.beer_project.models.utility.Country;
import com.example.beer_project.models.utility.Style;

import java.util.List;

public interface BeerRepository {

    void create(Beer beer);

    void update(long id, Beer beer);

    List<Beer> getAll();

    Beer getByBeerName(String name);

    Beer getById(long id);

    List<Beer> getPage(int page,String criteria,String order);

    Long getMaxPages();

    Long getMaxPagesByCriteria(String criteria);

    List<Beer> filterByCountry(Country country);

    List<Beer> filterByStyle(Style style);

    void delete(long id);
}
