package com.example.beer_project.repositories.utility;

import com.example.beer_project.models.Beer;
import com.example.beer_project.models.User;

public interface RatingRepository {
    void rate(User user, Beer beer, double rating);

    double calculateRating(Beer beer);
}
