package com.example.beer_project.repositories.utility;

import com.example.beer_project.models.utility.Style;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class StyleRepositoryImpl implements StyleRepository {
    private SessionFactory sessionFactory;

    public StyleRepositoryImpl(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    public List<Style> getAll() {
        Session session = sessionFactory.getCurrentSession();
        Query<Style> query = session.createQuery("from Style ", Style.class);

        return query.list();
    }

    @Override
    public Style getByName(String name) {
        Session session = sessionFactory.getCurrentSession();
        Query<Style> query = session.createQuery("from Style where name = :name", Style.class);
        query.setParameter("name", name);
        if (query.list().isEmpty()) return null;

        return query.list().get(0);
    }

}
