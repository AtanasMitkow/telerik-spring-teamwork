package com.example.beer_project.repositories;

import com.example.beer_project.models.Beer;
import com.example.beer_project.models.BeerDetails;
import com.example.beer_project.models.utility.Country;
import com.example.beer_project.models.utility.Style;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class BeerRepositoryImpl implements BeerRepository {
    private static final int BEERS_PER_PAGE=8;
    private SessionFactory sessionFactory;

    @Autowired
    public BeerRepositoryImpl(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    public void create(Beer beer) {
        Session session = sessionFactory.getCurrentSession();
        session.save(beer);
    }

    @Override
    public void update(long id, Beer beer) {

        Session session = sessionFactory.getCurrentSession();
        Beer beerToUpdate = getById(id);
        BeerDetails beerDetailsToUpdate = beerToUpdate.getBeerDetails();

        beerToUpdate.setDescription(beer.getDescription());
        beerToUpdate.setPicture(beer.getPicture());
        beerDetailsToUpdate.setBrewery(beer.getBeerDetails().getBrewery());
        beerDetailsToUpdate.setAbv(beer.getBeerDetails().getAbv());

        session.beginTransaction();
        session.update(beerToUpdate);
        session.update(beerDetailsToUpdate);
        session.getTransaction().commit();
    }

    @Override
    public List<Beer> getAll() {
        Session session = sessionFactory.getCurrentSession();
        Query<Beer> query = session.createQuery("from Beer where is_deleted is not true", Beer.class);

        return query.list();
    }

    @Override
    public Beer getById(long id) {
        Session session = sessionFactory.getCurrentSession();
        Query<Beer> query = session.createQuery("from Beer where id = :id and is_deleted is not true", Beer.class);
        query.setParameter("id", id);
        if (query.list().isEmpty()) return null;

        return query.list().get(0);
    }

    public Beer getByBeerName(String name) {
        Session session = sessionFactory.getCurrentSession();
        Query<Beer> query = session.createQuery("from Beer where name = :name and is_deleted is not true", Beer.class);
        query.setParameter("name", name);
        if (query.list().isEmpty()) return null;

        return query.list().get(0);
    }

    @Override
    public List<Beer> getPage(int page,String criteria,String order) {
        String[] params = order.split(" ");
        Session session = sessionFactory.getCurrentSession();
        Query<Beer> query = session.createQuery("from Beer  where is_deleted is not true " +
                "AND name like concat('%',:criteria,'%') order by " + order,Beer.class);
        query.setParameter("criteria",criteria);
        query.setFirstResult((page - 1) * BEERS_PER_PAGE);
        query.setMaxResults(BEERS_PER_PAGE);

        return query.list();
    }

    @Override
    public Long getMaxPages() {
        Session session = sessionFactory.getCurrentSession();
        Query<Long> query = session.createQuery("select count(*) from Beer where is_deleted is not true",Long.class);

        return query.list().get(0);
    }

    @Override
    public Long getMaxPagesByCriteria(String criteria) {
        Session session = sessionFactory.getCurrentSession();
        Query<Long> query = session.createQuery("select count(*) from Beer where is_deleted is not true" +
                " AND name like concat('%',:criteria,'%')",Long.class);
        query.setParameter("criteria",criteria);
        return query.list().get(0);
    }

    @Override
    public List<Beer> filterByCountry(Country country) {
        Session session = sessionFactory.getCurrentSession();
        Query<Beer> query = session.createQuery("from Beer where is_deleted is not true AND " +
                "beer_details_id in (from BeerDetails where country_id = :countryid)",Beer.class);
        query.setParameter("countryid",country.getId());
        return query.list();
    }

    public List<Beer> filterByStyle(Style style)
    {
        Session session = sessionFactory.getCurrentSession();
        Query<Beer> query = session.createQuery("from Beer where is_deleted is not true AND " +
                "beer_details_id in (from BeerDetails where style_id = :styleid)",Beer.class);
        query.setParameter("styleid",style.getId());
        return query.list();
    }

    @Override
    public void delete(long id) {
        Session session = sessionFactory.getCurrentSession();
        Beer beer = getById(id);
        session.beginTransaction();
        beer.setDeleted(true);
        beer.setName("[deleted]" + beer.getName());
        session.update(beer);
        session.getTransaction().commit();
    }
}
