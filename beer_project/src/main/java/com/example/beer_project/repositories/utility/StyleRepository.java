package com.example.beer_project.repositories.utility;

import com.example.beer_project.models.utility.Style;

import java.util.List;

public interface StyleRepository {
    List<Style> getAll();

    Style getByName(String name);

}
