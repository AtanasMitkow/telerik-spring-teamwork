package com.example.beer_project.repositories.utility;

import com.example.beer_project.models.utility.Country;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class CountryRepositoryImpl implements CountryRepository {
    private SessionFactory sessionFactory;

    public CountryRepositoryImpl(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    public List<Country> getAll() {
        Session session = sessionFactory.getCurrentSession();
        Query<Country> query = session.createQuery("from Country", Country.class);
        return query.list();
    }

    @Override
    public Country getByName(String name) {
        Session session = sessionFactory.getCurrentSession();
        Query<Country> query = session.createQuery("from Country where name = :name", Country.class);
        query.setParameter("name", name);
        if(query.list().isEmpty()) return null;
        return query.list().get(0);
    }

}
