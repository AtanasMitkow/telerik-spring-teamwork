package com.example.beer_project.repositories.utility;


import com.example.beer_project.models.Beer;
import com.example.beer_project.models.User;
import com.example.beer_project.models.middletable.DrankBeers;
import com.example.beer_project.models.middletable.DrankBeersID;
import org.hibernate.query.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.text.DecimalFormat;
import java.text.NumberFormat;

@Repository
public class RatingRepositoryImpl implements RatingRepository {
    private SessionFactory sessionFactory;

    @Autowired
    public RatingRepositoryImpl(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }


    @Override
    public void rate(User user, Beer beer, double rating) {
        DrankBeersID id = new DrankBeersID();
        id.setUserId(user.getId());
        id.setBeerId(beer.getId());
        DrankBeers rat = new DrankBeers();
        rat.setId(id);
        rat.setRating(rating);
        Session session = sessionFactory.getCurrentSession();
        session.beginTransaction();
        session.update(rat);
        session.getTransaction().commit();
    }

    @Override
    public double calculateRating(Beer beer) {
        Session session = sessionFactory.getCurrentSession();
        Query<DrankBeers> query = session.createQuery("from DrankBeers where beer_id= :id and rating is not null", DrankBeers.class);
        query.setParameter("id", beer.getId());
        double rating = (query.list().stream().mapToDouble(DrankBeers::getRating).sum()) / query.list().size();
        NumberFormat formatter = new DecimalFormat("#0.00");
        String ratingStr = formatter.format(rating);
        ratingStr = ratingStr.replace(',', '.');
        rating = Double.parseDouble(ratingStr);
        session.beginTransaction();
        beer.setRating(rating);
        session.update(beer);
        session.getTransaction().commit();

        return rating;
    }
}
