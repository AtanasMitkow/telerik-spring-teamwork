package com.example.beer_project.repositories;

import com.example.beer_project.models.Beer;
import com.example.beer_project.models.User;
import com.example.beer_project.models.UserDetails;
import com.example.beer_project.models.middletable.DrankBeers;
import org.hibernate.query.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;


@Repository
public class UserRepositoryImpl implements UserRepository {
    private static final int BEERS_PER_PAGE = 8;
    private static final int TOP_BEERS_NUMBER=3;
    private SessionFactory factory;

    @Autowired
    public UserRepositoryImpl(SessionFactory factory) {
        this.factory = factory;
    }

    @Override
    public void create(User user) {
        Session session = factory.getCurrentSession();
        session.save(user);
    }

    @Override
    public void update(long id, User user) {
        User toUpdate = getById(id);
        toUpdate.setPassword(user.getPassword());
        toUpdate.setRole(user.getRole());
        UserDetails detailsToUpdate = toUpdate.getUserDetails();
        detailsToUpdate.setFirstName(user.getUserDetails().getFirstName());
        detailsToUpdate.setLastName(user.getUserDetails().getLastName());
        detailsToUpdate.setPicture(user.getUserDetails().getPicture());
        Session session = factory.getCurrentSession();
        session.beginTransaction();
        session.update(detailsToUpdate);
        session.update(toUpdate);
        session.getTransaction().commit();
    }

    @Override
    public List<User> getAll() {
        Session session = factory.getCurrentSession();
        Query<User> query = session.createQuery("from User where enabled is true", User.class);

        return query.list();
    }

    @Override
    public User getByUserName(String name) {
        Session session = factory.getCurrentSession();
        Query<User> query = session.createQuery("from User where username = :name and enabled is true", User.class);
        query.setParameter("name", name);
        if (query.list().isEmpty()) return null;
        else return query.list().get(0);
    }

    @Override
    public User getById(long id) {
        Session session = factory.getCurrentSession();
        Query<User> query = session.createQuery("from User where id=:id and enabled is true", User.class);
        query.setParameter("id", id);
        if (query.list().isEmpty()) return null;
        else return query.list().get(0);
    }

    @Override
    public User getByEmail(String email) {
        Session session = factory.getCurrentSession();
        Query<User> query = session.createQuery("from User where email = :email and enabled is true", User.class);
        query.setParameter("email", email);
        if (query.list().isEmpty()) return null;
        else return query.list().get(0);
    }

    @Override
    public Set<Beer> getWishlist(User user) {
        return user.getWishlist();
    }

    @Override
    public Set<Beer> getDranklist(User user) {
        return user.getDrankBeers();
    }

    @Override
    public List<Long> top3Beers(long id) {
        Session session = factory.getCurrentSession();
        Query<DrankBeers> query = session.createQuery("FROM DrankBeers  where user_id = :user AND rating is not null ORDER BY rating desc ",DrankBeers.class);
        query.setParameter("user", id);
        query.setMaxResults(TOP_BEERS_NUMBER);
        List<DrankBeers> beers = query.list();
        List<Long> top3 = new ArrayList<>();
        beers.forEach(d -> top3.add(d.getId().getBeerId()));

        return top3;
    }

    @Override
    public Long getMaxPages() {
        Session session = factory.getCurrentSession();
        Query<Long> query = session.createQuery("select count(*) from User where enabled is true",Long.class);
        return query.list().get(0);
    }

    @Override
    public List<User> getPage(int page) {
        Session session = factory.getCurrentSession();
        Query<User> query = session.createQuery("from User where enabled is true order by user_id desc", User.class);
        query.setFirstResult((page - 1) * BEERS_PER_PAGE);
        query.setMaxResults(BEERS_PER_PAGE);
        return query.list();
    }

    @Override
    public void delete(User user) {
        Session session = factory.getCurrentSession();
        session.beginTransaction();
        session.update(user);
        session.getTransaction().commit();
    }
}
