package com.example.beer_project.utils;

public class Helper {
    private static final int MIN_PASSWORD_LENGTH=8;
    private static final int MAX_PASSWORD_LENGTH=18;
    public static String parseOrder(String order)
    {
        order = order.replace('(',' ');
        order = order.substring(0,order.length()-1);
        return order;
    }

    public static boolean passwordValidation(String password)
    {
        return password.length() >= MIN_PASSWORD_LENGTH && password.length() <= MAX_PASSWORD_LENGTH;
    }
}
