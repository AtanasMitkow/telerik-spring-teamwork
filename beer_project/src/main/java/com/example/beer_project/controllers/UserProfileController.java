package com.example.beer_project.controllers;


import com.example.beer_project.DTO.RegistrationDTO;
import com.example.beer_project.DTO.UserDTO;
import com.example.beer_project.exceptions.EmailAlreadyInUseException;
import com.example.beer_project.exceptions.PasswordOutOfRangeException;
import com.example.beer_project.exceptions.UserDoesntExistException;
import com.example.beer_project.exceptions.UsernameAlreadyInUseException;
import com.example.beer_project.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import java.util.List;

@RestController
@RequestMapping("/api")
public class UserProfileController {
    private UserService userService;

    @Autowired
    public UserProfileController(UserService userService) {
        this.userService = userService;
    }

    @PostMapping("/register")
    public void register(@RequestBody RegistrationDTO userDTO) {
        try {
            userService.registerUser(userDTO);
        } catch (IllegalArgumentException | PasswordOutOfRangeException e) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, e.getMessage());
        } catch (UsernameAlreadyInUseException | EmailAlreadyInUseException e) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, e.getMessage());
        }
    }

    @GetMapping("/profiles/{name}")
    public UserDTO getProfile(@PathVariable String name) {
        try {
            return userService.getProfile(name);
        } catch (UserDoesntExistException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

    @GetMapping("/profiles")
    public List<UserDTO> getProfiles(@RequestParam int page) {
        try {
            return userService.listUsers(page);
        } catch (IllegalArgumentException e) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, e.getMessage());
        }
    }

    @PutMapping("/profiles/edit/{id}")
    public void editProfile(@PathVariable long id, @RequestBody RegistrationDTO registrationDTO) {
        try {
            userService.updateProfile(id, registrationDTO);
        } catch (UserDoesntExistException | PasswordOutOfRangeException e) {
            throw new ResponseStatusException(
                    HttpStatus.BAD_REQUEST, e.getMessage()
            );
        }
    }
}
