package com.example.beer_project.controllers;

import com.example.beer_project.DTO.BeerDisplayDTO;
import com.example.beer_project.exceptions.*;
import com.example.beer_project.models.User;
import com.example.beer_project.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import java.util.List;

@RestController
@RequestMapping("/api/users")
public class UserController {
    private UserService userService;

    @Autowired
    public UserController(UserService service) {
        this.userService = service;
    }

    @PostMapping("/new")
    public void createUser(@RequestBody User user) {
        try {
            userService.create(user);
        } catch (UsernameAlreadyInUseException e) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, e.getMessage());
        }
    }

    @PutMapping("/{id}")
    public void updateUser(@PathVariable long id, @RequestBody User user) {
        try {
            userService.update(id, user);
        } catch (UserDoesntExistException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

    @GetMapping
    public List<User> getAll() {
        return userService.getAll();
    }

    @GetMapping("/{id}")
    public User getById(@PathVariable long id) {
        try {
            return userService.getById(id);
        } catch (UserDoesntExistException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

    @GetMapping("/pages")
    public Long getMaxPages() {
        return userService.getMaxPages();
    }

    @GetMapping("/{userID}/wishlist")
    public List<BeerDisplayDTO> getUserWishlist(@PathVariable long userID) {
        try {
            return userService.getWishlist(userID);
        } catch (UserDoesntExistException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

    @GetMapping("/{userID}/dranklist")
    public List<BeerDisplayDTO> getDrankList(@PathVariable long userID) {
        try {
            return userService.getDranklist(userID);
        } catch (UserDoesntExistException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

    @GetMapping("/{userID}/top3")
    public List<BeerDisplayDTO> top3beers(@PathVariable long userID) {
        try {
            return userService.top3Beers(userID);
        } catch (UserDoesntExistException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

    @PutMapping("/{userID}/wishBeer/{beer}")
    public void addBeerToWishlist(@PathVariable long userID, @PathVariable String beer) {
        try {
            userService.addBeerToWishlist(userID, beer);
        } catch (BeerDoesntExistException | UserDoesntExistException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        } catch (BeerAlreadyInWishlistException e) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, e.getMessage());
        }
    }

    @PutMapping("/{userID}/drinkBeer/{beer}")
    public void addBeerToDrinklist(@PathVariable long userID, @PathVariable String beer) {
        try {
            userService.addBeerToDrinklist(userID, beer);
        } catch (BeerDoesntExistException | UserDoesntExistException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        } catch (BeerAlreadyInWishlistException e) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, e.getMessage());
        }
    }

    @DeleteMapping("/{id}")
    public void deleteById(@PathVariable long id) {
        try {
            userService.delete(id);
        } catch (UserDoesntExistException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }
}
