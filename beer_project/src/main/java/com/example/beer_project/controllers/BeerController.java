package com.example.beer_project.controllers;

import com.example.beer_project.DTO.BeerDTO;
import com.example.beer_project.DTO.BeerDisplayDTO;
import com.example.beer_project.DTO.BeerUpdateDTO;
import com.example.beer_project.exceptions.*;
import com.example.beer_project.models.Beer;
import com.example.beer_project.services.BeerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.validation.ConstraintViolationException;
import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/api/beers")
public class BeerController {
    private BeerService beerService;

    @Autowired
    public BeerController(BeerService service) {
        this.beerService = service;
    }

    @PostMapping("/new")
    public BeerDTO create(@Valid @RequestBody BeerDTO beer) {
        try {
            beerService.create(beer);

        } catch (BeerWithNameAlreadyExistException e) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, e.getMessage());
        } catch (ConstraintViolationException e) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, e.getMessage());
        }
        //function addTagToTagList contains update func that throws this exception
        catch (BeerDoesntExistException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
        return beer;
    }

    @PutMapping("/{beerName}")
    public void update(@PathVariable String beerName, @RequestBody BeerUpdateDTO beer) {
        try {
            beerService.update(beerName, beer);
        } catch (BeerDoesntExistException e) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, e.getMessage());
        }
    }

    @GetMapping("/all")
    public List<Beer> getAll() {
        return beerService.getAll();
    }

    @GetMapping
    public BeerDTO getByBeerName(@RequestParam String name) {
        try {
            beerService.getByBeerName(name);
            return beerService.getByBeerName(name);
        } catch (BeerDoesntExistException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

    @GetMapping("/{id}")
    public Beer getById(@PathVariable @Valid long id) {
        try {
            return beerService.getById(id);
        } catch (BeerDoesntExistException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

    @GetMapping("/profiles")
    public List<BeerDisplayDTO> getBeerProfiles(@RequestParam(required = false) Integer page, @RequestParam(required = false) String crit, @RequestParam(required = false) String order) {
        try {
            return beerService.listBeers(page, crit, order);
        } catch (IllegalArgumentException e) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, e.getMessage());
        }
    }

    @GetMapping("/pages")
    public Long getMaxPages(@RequestParam(required = false) String crit) {
        return beerService.getMaxPages(crit);
    }

    @PutMapping("/{beerName}/rate/{userid}/{rating}")
    public void rateBeer(@PathVariable String beerName, @PathVariable long userid, @PathVariable double rating) {
        try {
            beerService.rate(beerName, userid, rating);
        } catch (CantRateNotTastedBeerException e) {
            throw new ResponseStatusException(
                    HttpStatus.FORBIDDEN,
                    e.getMessage());
        } catch (BeerDoesntExistException | UserDoesntExistException ex) {
            throw new ResponseStatusException(
                    HttpStatus.NOT_FOUND, ex.getMessage()
            );
        } catch (RatingRangeException e) {
            throw new ResponseStatusException(
                    HttpStatus.BAD_REQUEST, e.getMessage()
            );
        }
    }

    @PutMapping("/{beerName}/addTag")
    public void addTagToTagList(@RequestParam String tagName, @PathVariable String beerName) {
        try {
            beerService.addTagToTagList(tagName, beerName);
        } catch (BeerDoesntExistException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

    @GetMapping("/filter")
    public List<BeerDisplayDTO> filterBeers(@RequestParam String filter) {
        return beerService.filterBeers(filter);
    }

    @DeleteMapping("/{id}")
    public void delete(@PathVariable long id) {
        try {
            beerService.delete(id);
        } catch (BeerDoesntExistException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }
}
