package com.example.beer_project.controllers.tlcontrollers;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

import java.security.Principal;

@Controller
public class CurrentUserController {
    @GetMapping("/me")
    public String getCurrentUser(Principal principal) {
        if (principal == null) return "redirect:/";
        return "redirect:/api/profiles/" + principal.getName();
    }
}
