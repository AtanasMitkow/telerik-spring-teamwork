package com.example.beer_project.controllers;

import com.example.beer_project.models.utility.Country;
import com.example.beer_project.models.utility.Style;
import com.example.beer_project.services.UtilityService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/api/utility")
public class UtilityController {
    private UtilityService utilityService;

    @Autowired
    public UtilityController(UtilityService utilityService) {
        this.utilityService = utilityService;
    }

    @GetMapping("/countries")
    public List<Country> getCountries()
    {
        return utilityService.getAllCountries();
    }

    @GetMapping("/styles")
    public List<Style> getStyles()
    {
        return utilityService.getAllStyles();
    }
}
