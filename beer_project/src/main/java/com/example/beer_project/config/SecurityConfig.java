package com.example.beer_project.config;

import com.example.beer_project.controllers.tlcontrollers.CustomAccessDeniedHandler;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.factory.PasswordEncoderFactories;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.access.AccessDeniedHandler;

import javax.sql.DataSource;

@Configuration
@EnableWebSecurity
@PropertySource("classpath:application.properties")
public class SecurityConfig extends WebSecurityConfigurerAdapter {
    private String url, name, password;
    private DataSource securityDataSource;

    @Autowired
    public SecurityConfig(Environment environment, DataSource dataSource) {
        securityDataSource = dataSource;
        this.url = environment.getProperty("database.url");
        this.name = environment.getProperty("database.username");
        this.password = environment.getProperty("database.password");
    }

    @Override
    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
        PasswordEncoder encoder = PasswordEncoderFactories.createDelegatingPasswordEncoder();
        auth.jdbcAuthentication().dataSource(this.securityDataSource)
                .usersByUsernameQuery("SELECT username, password, enabled FROM users where username = ?")
                .authoritiesByUsernameQuery("select username, authority from users where username=?");
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http.csrf().disable()
                .authorizeRequests()
                .antMatchers("/api/register").anonymous()
                .antMatchers("/css/*").permitAll()
                .antMatchers("/scripts/*").permitAll()
                .antMatchers("/img/*").permitAll()
                .antMatchers(HttpMethod.DELETE,"/api/beers/**").authenticated()
                .antMatchers(HttpMethod.POST,"/api/beers/**").authenticated()
                .antMatchers(HttpMethod.PUT,"api/beers/**").authenticated()
                .antMatchers("/api/beers/**").permitAll()
                .antMatchers("/api/utility/**").permitAll()
                .antMatchers("/api/users/**/wishBeer/**").authenticated()
                .antMatchers("/api/users/**/drinkBeer/**").authenticated()
                .antMatchers("/api/users/**/dranklist/**").authenticated()
                .antMatchers("/api/users/**/wishlist/**").authenticated()
                .antMatchers("/api/users/**/top3/**").authenticated()
                .antMatchers("/api/users/**").hasRole("ADMIN")
                .antMatchers("/api/profiles").hasRole("ADMIN")
                .anyRequest().authenticated()
                .antMatchers("/api/**").authenticated()
                .and().formLogin().loginPage("/login")
                .loginProcessingUrl("/authenticate").permitAll().defaultSuccessUrl("/")
                .and().exceptionHandling().accessDeniedPage("/").accessDeniedHandler(accessDeniedHandler())
                .and().logout().permitAll()
                .logoutUrl("/logout").logoutSuccessUrl("/");

    }

    @Bean
    public PasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder();
    }

    @Bean
    public AccessDeniedHandler accessDeniedHandler()
    {
        return new CustomAccessDeniedHandler();
    }
}
