package com.example.beer_project.services;

import com.example.beer_project.DTO.BeerDTO;
import com.example.beer_project.DTO.BeerDisplayDTO;
import com.example.beer_project.DTO.BeerUpdateDTO;
import com.example.beer_project.exceptions.*;
import com.example.beer_project.models.Beer;

import java.util.List;

public interface BeerService {

    void create(BeerDTO beer) throws BeerWithNameAlreadyExistException, BeerDoesntExistException;

    void update(String beerName, BeerUpdateDTO beer) throws BeerDoesntExistException;

    List<Beer> getAll();

    BeerDTO getByBeerName(String name) throws BeerDoesntExistException;

    Beer getById(long id) throws BeerDoesntExistException;

    BeerDisplayDTO beerToDisplayDTO(Beer beer);

    List<BeerDisplayDTO> listBeers(Integer page,String crit,String order);

    Long getMaxPages(String criteria);

    void addTagToTagList(String tagName, String beerName) throws BeerDoesntExistException;

    BeerDTO beerToDTO(Beer beer);

    void rate(String beerName, long userid, double rating) throws CantRateNotTastedBeerException, BeerDoesntExistException, RatingRangeException, UserDoesntExistException;

    List<BeerDisplayDTO> filterBeers(String filter);

    void delete(long id) throws BeerDoesntExistException;
}
