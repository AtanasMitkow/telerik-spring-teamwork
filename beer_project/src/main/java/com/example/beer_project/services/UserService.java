package com.example.beer_project.services;

import com.example.beer_project.DTO.BeerDisplayDTO;
import com.example.beer_project.DTO.RegistrationDTO;
import com.example.beer_project.DTO.UserDTO;
import com.example.beer_project.exceptions.*;
import com.example.beer_project.models.User;

import java.util.List;

public interface UserService {
    void create(User user) throws UsernameAlreadyInUseException;

    void registerUser(RegistrationDTO userDTO) throws UsernameAlreadyInUseException, EmailAlreadyInUseException, PasswordOutOfRangeException;

    void updateProfile(long id, RegistrationDTO registrationDTO) throws UserDoesntExistException, PasswordOutOfRangeException;

    void update(long id, User user) throws UserDoesntExistException;

    List<User> getAll();

    User getById(long id) throws UserDoesntExistException;

    User getByUserName(String username) throws UserDoesntExistException;

    UserDTO getProfile(String username) throws UserDoesntExistException;

    List<UserDTO> getProfiles();

    List<BeerDisplayDTO> getWishlist(long userId) throws UserDoesntExistException;

    List<BeerDisplayDTO> getDranklist(long userId) throws UserDoesntExistException;

    List<BeerDisplayDTO> top3Beers(long id) throws UserDoesntExistException;

    Long getMaxPages();

    List<UserDTO> listUsers(int page);

    void addBeerToWishlist(long userId, String beerName) throws UserDoesntExistException, BeerDoesntExistException, BeerAlreadyInWishlistException;

    void addBeerToDrinklist(long userId, String beer) throws BeerDoesntExistException, UserDoesntExistException, BeerAlreadyInWishlistException;

    UserDTO userToDTO(User user);

    void delete(long id) throws UserDoesntExistException;
}