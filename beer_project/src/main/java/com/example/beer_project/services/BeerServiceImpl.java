package com.example.beer_project.services;

import com.example.beer_project.DTO.BeerDTO;
import com.example.beer_project.DTO.BeerDisplayDTO;
import com.example.beer_project.DTO.BeerUpdateDTO;
import com.example.beer_project.exceptions.*;
import com.example.beer_project.models.Beer;
import com.example.beer_project.models.BeerDetails;
import com.example.beer_project.models.User;
import com.example.beer_project.models.utility.Country;
import com.example.beer_project.models.utility.Picture;
import com.example.beer_project.models.utility.Style;
import com.example.beer_project.models.utility.Tag;
import com.example.beer_project.repositories.BeerRepository;
import com.example.beer_project.repositories.UserRepository;
import com.example.beer_project.repositories.utility.*;
import com.example.beer_project.utils.Helper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class BeerServiceImpl implements BeerService {
    private static final int PAGE_SIZE = 8;
    private static final int BEER_MIN_URL_SIZE = 15;
    private static final String BEER_DEFAULT_PICTURE = "img/beer.png";

    private BeerRepository repository;
    private UserRepository userRepository;
    private RatingRepository ratingRepository;
    private CountryRepository countryRepository;
    private StyleRepository styleRepository;

    @Autowired
    public BeerServiceImpl(BeerRepository repository, UserRepository userRepository, RatingRepository ratingRepository, CountryRepository countryRepository, StyleRepository styleRepository) {
        this.repository = repository;
        this.userRepository = userRepository;
        this.ratingRepository = ratingRepository;
        this.countryRepository = countryRepository;
        this.styleRepository = styleRepository;
    }

    @Override
    public void create(BeerDTO beerDTO) throws BeerWithNameAlreadyExistException, BeerDoesntExistException {

        if (repository.getByBeerName(beerDTO.getName()) != null) {
            throw new BeerWithNameAlreadyExistException(
                    String.format("Beer with name %s already exist!", beerDTO.getName())
            );
        }
        Beer beerToCreate = new Beer();
        BeerDetails beerDetailsToCreate = new BeerDetails();
        Picture picture = new Picture();
        if(beerDTO.getPicture().length() < BEER_MIN_URL_SIZE) picture.setUrl(BEER_DEFAULT_PICTURE);
        else picture.setUrl(beerDTO.getPicture());
        beerToCreate.setName(beerDTO.getName());
        beerToCreate.setDescription(beerDTO.getDescription());
        beerToCreate.setPicture(picture);
        beerToCreate.setCreator(userRepository.getByUserName(beerDTO.getCreator()));
        beerToCreate.setDeleted(false);
        beerDetailsToCreate.setBrewery(beerDTO.getBrewery());
        beerDetailsToCreate.setAbv(beerDTO.getAbv());
        beerDetailsToCreate.setCountry(countryRepository.getByName(beerDTO.getCountry()));
        beerDetailsToCreate.setStyle(styleRepository.getByName(beerDTO.getStyle()));
        beerToCreate.setBeerDetails(beerDetailsToCreate);

        repository.create(beerToCreate);

        String[] tags = beerDTO.getTag().split(",");
        int size = tags.length;
        if (size > 5) {
            size = 5;
        }
        for (int i = 0; i < size; i++) {
            Tag tag = new Tag(tags[i]);
            addTagToTagList(tag.getName(), beerDTO.getName());
        }
    }

    @Override
    public void update(String beerName, BeerUpdateDTO beer) throws BeerDoesntExistException {
        Beer beer1 = repository.getByBeerName(beerName);
        if (beer1 == null)
            throw new BeerDoesntExistException(String.format("Beer with name %s does not exist", beerName));
        beer1.setDescription(beer.getDescription());
        beer1.getBeerDetails().setAbv(beer.getAbv());
        beer1.getBeerDetails().setBrewery(beer.getBrewery());
        Picture picture = new Picture();
        if(beer.getPicture().length() < BEER_MIN_URL_SIZE) picture.setUrl(BEER_DEFAULT_PICTURE);
        else picture.setUrl(beer.getPicture());
        beer1.setPicture(picture);
        long id = beer1.getId();
        repository.update(id, beer1);
    }

    @Override
    public List<Beer> getAll() {
        return repository.getAll();
    }

    @Override
    public BeerDTO getByBeerName(String name) throws BeerDoesntExistException {

        if (repository.getByBeerName(name) == null) {
            throw new BeerDoesntExistException(
                    String.format("Beer with name %s not found!", name)
            );
        }
        return beerToDTO(repository.getByBeerName(name));
    }

    @Override
    public Beer getById(long id) throws BeerDoesntExistException {

        if (repository.getById(id) == null) {
            throw new BeerDoesntExistException(
                    String.format("Beer with id %d not found", id)
            );
        }
        return repository.getById(id);
    }

    @Override
    public List<BeerDisplayDTO> listBeers(Integer page,String crit,String order) {
        List<Beer> beers = new ArrayList<>();
        List<BeerDisplayDTO> list = new ArrayList<>();
        if(crit == null && page == null && order == null)
        {
            beers =  repository.getAll();
        }
        else {
            Long maxPages;
            if (crit == null) {
                maxPages = repository.getMaxPages();
                crit = ""; // In order to not pass null to the repository "" fileters by nothing
            } else maxPages = repository.getMaxPagesByCriteria(crit);
            if (page > (maxPages / PAGE_SIZE + 1))
                throw new IllegalArgumentException("Page does not exist");
            order = Helper.parseOrder(order);
            beers = repository.getPage(page,crit,order);
        }
        beers.forEach(beer -> list.add(beerToDisplayDTO(beer)));
        return list;
    }

    @Override
    public Long getMaxPages(String criteria) {
        if(criteria==null)
            return repository.getMaxPages() / PAGE_SIZE + 1;
        else return repository.getMaxPagesByCriteria(criteria)/PAGE_SIZE + 1;
    }

    @Override
    public void rate(String beerName, long userid, double rating) throws UserDoesntExistException,
            RatingRangeException, CantRateNotTastedBeerException, BeerDoesntExistException {
        Beer beer = repository.getByBeerName(beerName);
        User user = userRepository.getById(userid);
        if (beer == null)
            throw new BeerDoesntExistException(String.format("Beer with name %s does not exist", beerName));
        if (user == null) throw new UserDoesntExistException("User does not exist!");
        if (rating < 1 || rating > 5) throw new RatingRangeException("Rating must be between 1 and 5");
        if (user.getDrankBeers().contains(beer)) {
            ratingRepository.rate(user, beer, rating);
        } else throw new CantRateNotTastedBeerException("You cant rate for a beer you have not tasted");

        ratingRepository.calculateRating(beer);
    }

    @Override
    public List<BeerDisplayDTO> filterBeers(String filter) {
        filter = Helper.parseOrder(filter);
        String[] params = filter.split(" ");
        List<Beer> beers = new ArrayList<>();
        switch(params[1].toUpperCase())
        {
            case "TAG":
            {
                Tag tag = new Tag(params[0]);
                List<Beer> allBeers = repository.getAll();
                for (Beer beer: allBeers) {
                    if(beer.getTag().contains(tag)) beers.add(beer);
                }
                break;
            }
            case "COUNTRY":
            {
                Country country = countryRepository.getByName(params[0]);
                if(country ==null) return new ArrayList<>();
                beers = repository.filterByCountry(country);
                break;
            }
            case "STYLE":
            {
                Style style = styleRepository.getByName(params[0]);
                if(style == null) return new ArrayList<>();
                beers = repository.filterByStyle(style);
                break;
            }
        }
        List<BeerDisplayDTO> list = new ArrayList<>();
        beers.forEach(beer -> list.add(beerToDisplayDTO(beer)));
        return list;
    }

    @Override
    public void addTagToTagList(String tagName, String beerName) throws BeerDoesntExistException {
        Beer beer = repository.getByBeerName(beerName);
        if (beer == null) throw new BeerDoesntExistException("Beer not found!");
        Tag tag = new Tag(tagName);
        beer.getTag().add(tag);
        repository.update(beer.getId(), beer);
    }

    @Override
    public BeerDTO beerToDTO(Beer beer) {
        BeerDTO beerDTO = new BeerDTO();
        beerDTO.setName(beer.getName());
        beerDTO.setDescription(beer.getDescription());
        beerDTO.setCountry(beer.getBeerDetails().getCountry().getName());
        beerDTO.setStyle(beer.getBeerDetails().getStyle().getName());
        beerDTO.setBrewery(beer.getBeerDetails().getBrewery());
        String tags = "";
        if (beer.getTag().isEmpty()) tags = "";
        else {
            tags = beer.getTag().toString();
            tags = tags.substring(1, tags.length() - 1);
        }
        beerDTO.setTag(tags);
        beerDTO.setAbv(beer.getBeerDetails().getAbv());
        beerDTO.setRating(beer.getRating());
        beerDTO.setPicture(beer.getPicture().getUrl());
        beerDTO.setCreator(beer.getCreator().getUserDetails().getFirstName() + " " + beer.getCreator().getUserDetails().getLastName());
        return beerDTO;
    }

    @Override
    public BeerDisplayDTO beerToDisplayDTO(Beer beer) {
        BeerDisplayDTO beerDisplayDTO = new BeerDisplayDTO();
        beerDisplayDTO.setName(beer.getName());
        if (beer.getPicture() != null) {
            beerDisplayDTO.setPicture(beer.getPicture().getUrl());
        } else beerDisplayDTO.setPicture("default.picture");
        beerDisplayDTO.setAbv(beer.getBeerDetails().getAbv());
        beerDisplayDTO.setRating(beer.getRating());
        beerDisplayDTO.setDescription(beer.getDescription());
        String fullname = beer.getCreator().getUserDetails().getFirstName() + " " + beer.getCreator().getUserDetails().getLastName();
        beerDisplayDTO.setCreator(fullname);
        beerDisplayDTO.setStyleID(1);

        return beerDisplayDTO;
    }

    @Override
    public void delete(long id) throws BeerDoesntExistException {
        Beer beer = getById(id);
        if (beer == null) throw new BeerDoesntExistException(String.format("Beer with id %s does not exist", id));
        repository.delete(id);
    }
}
