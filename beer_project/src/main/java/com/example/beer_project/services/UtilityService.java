package com.example.beer_project.services;

import com.example.beer_project.models.utility.Country;
import com.example.beer_project.models.utility.Style;

import java.util.List;

public interface UtilityService {

    List<Country> getAllCountries();

    List<Style> getAllStyles();

}
