package com.example.beer_project.services;

import com.example.beer_project.DTO.BeerDisplayDTO;
import com.example.beer_project.DTO.RegistrationDTO;
import com.example.beer_project.DTO.UserDTO;
import com.example.beer_project.exceptions.*;
import com.example.beer_project.models.Beer;
import com.example.beer_project.models.User;
import com.example.beer_project.models.UserDetails;
import com.example.beer_project.models.utility.Country;
import com.example.beer_project.models.utility.Picture;
import com.example.beer_project.repositories.BeerRepository;
import com.example.beer_project.repositories.UserRepository;
import com.example.beer_project.repositories.utility.CountryRepository;
import com.example.beer_project.utils.Helper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.*;

@Service
public class UserServiceImpl implements UserService {
    private static final int MIN_LENGTH = 8;
    private static final int MAX_LENGTH = 18;
    private static final int PAGE_SIZE = 8;
    private static final int MIN_PIC_URL_LENGTH = 10;
    private static final String DEFAULT_AVATAR_URL = "img/user.png";

    private UserRepository userRepository;
    private CountryRepository countryRepository;
    private BeerRepository beerRepository;
    private BeerService beerService;
    private PasswordEncoder pwEncoder;


    @Autowired
    public UserServiceImpl(UserRepository userRepository, CountryRepository countryRepository, BeerRepository beerRepository, BeerService beerService, PasswordEncoder pwEncoder) {
        this.userRepository = userRepository;
        this.countryRepository = countryRepository;
        this.beerRepository = beerRepository;
        this.beerService=beerService;
        this.pwEncoder = pwEncoder;
    }

    @Override
    public void create(User user) throws UsernameAlreadyInUseException {
        String username = user.getUsername();
        User toCreate = userRepository.getByUserName(username);
        if(toCreate!=null) throw new UsernameAlreadyInUseException("Username already exists");
        userRepository.create(user);
    }

    @Override
    public void registerUser(RegistrationDTO userDTO) throws EmailAlreadyInUseException, UsernameAlreadyInUseException, PasswordOutOfRangeException {
        if(userDTO.getUsername().length()<MIN_LENGTH || userDTO.getUsername().length() > MAX_LENGTH)
            throw new IllegalArgumentException("Username must be between 8 and 18 symbols");
        User checkUser = userRepository.getByUserName(userDTO.getUsername());
        if(checkUser == null) checkUser = userRepository.getByEmail(userDTO.getEmail());
        else throw new UsernameAlreadyInUseException("Username already taken!");
        if(checkUser != null) throw new EmailAlreadyInUseException("Email already in use!");
        User user = new User();
        UserDetails details = new UserDetails();
        user.setRole("ROLE_USER");
        user.setUsername(userDTO.getUsername());
        user.setEmail(userDTO.getEmail());
        if(!Helper.passwordValidation(userDTO.getPassword())) throw new PasswordOutOfRangeException("Password must be between 8 and 18 symbols!");
        user.setPassword(pwEncoder.encode(userDTO.getPassword()));
        user.setEnabled(true);
        details.setFirstName(userDTO.getFirstName());
        details.setLastName(userDTO.getLastName());
        Picture picture = new Picture();
        if(userDTO.getPictureUrl().length()<MIN_PIC_URL_LENGTH) picture.setUrl(DEFAULT_AVATAR_URL);
        else picture.setUrl(userDTO.getPictureUrl());
        details.setPicture(picture);
        Country country = countryRepository.getByName(userDTO.getCountryName());
        details.setCountry(country);
        user.setUserDetails(details);
        userRepository.create(user);
    }

    @Override
    public void update(long id, User user) throws UserDoesntExistException{
        User toUpdate = userRepository.getById(id);
        if(toUpdate == null) throw new UserDoesntExistException(String.format("User with id %d does not exist",id));
        userRepository.update(id,user);
    }

    @Override
    public void updateProfile(long id,RegistrationDTO registrationDTO) throws UserDoesntExistException, PasswordOutOfRangeException {
        User user = userRepository.getById(id);
        if(user == null) throw new UserDoesntExistException("User not found!");
        if(!Helper.passwordValidation(registrationDTO.getPassword())) throw new PasswordOutOfRangeException("Password must be between 8 and 18 symbols!");
        user.setPassword(pwEncoder.encode(registrationDTO.getPassword()));
        user.getUserDetails().setFirstName(registrationDTO.getFirstName());
        user.getUserDetails().setLastName(registrationDTO.getLastName());
        if(registrationDTO.getPictureUrl().length()<MIN_PIC_URL_LENGTH) user.getUserDetails().getPicture().setUrl(DEFAULT_AVATAR_URL);
        else user.getUserDetails().getPicture().setUrl(registrationDTO.getPictureUrl());
        Country country = countryRepository.getByName(registrationDTO.getCountryName());
        user.getUserDetails().setCountry(country);
        userRepository.update(id,user);
    }

    @Override
    public List<User> getAll() {
        return userRepository.getAll();
    }

    @Override
    public User getById(long id) throws UserDoesntExistException{
        User user = userRepository.getById(id);
        if(user == null) throw new UserDoesntExistException(String.format("User with id %d does not exist",id));
        return user;
    }

    @Override
    public User getByUserName(String username) throws UserDoesntExistException{
        User user = userRepository.getByUserName(username);
        if(user == null) throw new UserDoesntExistException(String.format("User with name %s does not exist",username));
        return user;
    }

    @Override
    public UserDTO getProfile(String username) throws UserDoesntExistException {
        User user = getByUserName(username);
        return userToDTO(user);
    }

    @Override
    public List<UserDTO> getProfiles() {
        List<User> users = userRepository.getAll();
        List<UserDTO> profiles = new ArrayList<>();
        users.forEach(u -> profiles.add(userToDTO(u)));
        return profiles;
    }

    @Override
    public List<BeerDisplayDTO> getWishlist(long userId)throws UserDoesntExistException {
        User user = userRepository.getById(userId);
        if(user==null) throw new UserDoesntExistException(String.format("User with id %d not found",userId));
        List<BeerDisplayDTO> beerDisplayDTOS = new ArrayList<>();
        Set<Beer> beers = userRepository.getWishlist(user);
        beers.forEach(beer -> beerDisplayDTOS.add(beerService.beerToDisplayDTO(beer)));
        beerDisplayDTOS.sort(Comparator.comparing(BeerDisplayDTO::getName));
        return beerDisplayDTOS;
    }

    @Override
    public List<BeerDisplayDTO> getDranklist(long userId) throws UserDoesntExistException {
        User user = userRepository.getById(userId);
        if(user==null) throw new UserDoesntExistException(String.format("User with id %d not found",userId));
        Set<Beer> beers = userRepository.getDranklist(user);
        List<BeerDisplayDTO> beerDisplayDTOS = new ArrayList<>();
        beers.forEach(beer -> beerDisplayDTOS.add(beerService.beerToDisplayDTO(beer)));
        beerDisplayDTOS.sort(Comparator.comparing(BeerDisplayDTO::getName));
        return beerDisplayDTOS;
    }

    @Override
    public List<BeerDisplayDTO> top3Beers(long id) throws UserDoesntExistException {
        User user = userRepository.getById(id);
        if(user==null) throw new UserDoesntExistException(String.format("User with id %d not found",id));
        List<Beer> beers = new ArrayList<>();
        userRepository.top3Beers(id).forEach(d->beers.add(beerRepository.getById(d)));
        List<BeerDisplayDTO> beerDTOS = new ArrayList<>();
        beers.forEach(a->beerDTOS.add(beerService.beerToDisplayDTO(a)));
        return beerDTOS;
    }

    @Override
    public List<UserDTO> listUsers(int page) {
        if (page > (userRepository.getMaxPages() / PAGE_SIZE + 1))
            throw new IllegalArgumentException("Page does not exist");
        List<UserDTO> list = new ArrayList<>();
        List<User> users = userRepository.getPage(page);
        users.forEach(user -> list.add(userToDTO(user)));
        return list;
    }

    @Override
    public Long getMaxPages() {
        return userRepository.getMaxPages() / PAGE_SIZE + 1;
    }

    @Override
    public void addBeerToWishlist(long userId, String beerName) throws BeerDoesntExistException, UserDoesntExistException, BeerAlreadyInWishlistException {
        User user = getById(userId);
        Beer beer = beerRepository.getByBeerName(beerName);
        if(beer == null) throw new BeerDoesntExistException("Beer not found!");
        if(user.getWishlist().contains(beer)) throw new BeerAlreadyInWishlistException("Beer is already in your wishlist");
        user.addWishedBeer(beer);
        userRepository.update(userId,user);
    }

    @Override
    public void addBeerToDrinklist(long userId, String beerName) throws BeerDoesntExistException, UserDoesntExistException, BeerAlreadyInWishlistException {
        User user = getById(userId);
        Beer beer = beerRepository.getByBeerName(beerName);
        if(beer == null) throw new BeerDoesntExistException("Beer not found!");
        if(user.getDrankBeers().contains(beer)) throw new BeerAlreadyInWishlistException("Beer already in your dranklist");
        user.getDrankBeers().add(beer);
        userRepository.update(userId,user);
    }

    @Override
    public UserDTO userToDTO(User user)
    {
        UserDTO userDTO = new UserDTO();
        userDTO.setId(user.getId());
        userDTO.setRole(user.getRole());
        userDTO.setUsername(user.getUsername());
        userDTO.setEmail(user.getEmail());
        userDTO.setFirstName(user.getUserDetails().getFirstName());
        userDTO.setLastName(user.getUserDetails().getLastName());
        userDTO.setCountry(user.getUserDetails().getCountry().getName());
        if(user.getUserDetails().getPicture()!=null) {
            userDTO.setPicture(user.getUserDetails().getPicture().getUrl());
        } else userDTO.setPicture("default.picture");
        return userDTO;
    }

    @Override
    public void delete(long id) throws UserDoesntExistException{
        User user = getById(id);
        if(user == null) throw new UserDoesntExistException(String.format("User with id %d does not exist",id));
        user.setUsername("[deleted]" + user.getUsername());
        user.setEmail("[deleted]" + user.getEmail());
        user.setEnabled(false);
        userRepository.delete(user);
    }
}