package com.example.beer_project.services;

import com.example.beer_project.models.utility.Country;
import com.example.beer_project.models.utility.Style;
import com.example.beer_project.repositories.utility.CountryRepository;
import com.example.beer_project.repositories.utility.StyleRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class UtilityServiceImpl implements UtilityService {
    private CountryRepository countryRepository;
    private StyleRepository styleRepository;

    @Autowired
    public UtilityServiceImpl(CountryRepository countryRepository, StyleRepository styleRepository) {
        this.countryRepository = countryRepository;
        this.styleRepository = styleRepository;
    }

    @Override
    public List<Country> getAllCountries() {
        return countryRepository.getAll();
    }

    @Override
    public List<Style> getAllStyles() {
        return styleRepository.getAll();
    }
}
