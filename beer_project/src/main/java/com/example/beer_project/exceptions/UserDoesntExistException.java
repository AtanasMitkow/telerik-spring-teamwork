package com.example.beer_project.exceptions;

public class UserDoesntExistException extends Throwable {

    public UserDoesntExistException(String string) {
        super(string);
    }

}
