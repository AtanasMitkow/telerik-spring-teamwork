package com.example.beer_project.exceptions;

public class UsernameAlreadyInUseException extends Throwable {

    public UsernameAlreadyInUseException(String string) {
        super(string);
    }

}
