package com.example.beer_project.exceptions;

public class BeerDoesntExistException extends Throwable {

    public BeerDoesntExistException(String string) {
        super(string);
    }

}
