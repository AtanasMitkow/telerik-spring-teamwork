package com.example.beer_project.exceptions;

public class CantRateNotTastedBeerException extends Exception {

    public CantRateNotTastedBeerException(String string) {
        super(string);
    }

}
