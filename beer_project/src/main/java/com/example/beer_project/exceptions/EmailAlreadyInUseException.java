package com.example.beer_project.exceptions;

public class EmailAlreadyInUseException extends Throwable {

    public EmailAlreadyInUseException(String string) {
        super(string);
    }

}
