package com.example.beer_project.exceptions;

public class BeerAlreadyInWishlistException extends Throwable {

    public BeerAlreadyInWishlistException(String string) {
        super(string);
    }

}