package com.example.beer_project.exceptions;

public class BeerWithNameAlreadyExistException extends Throwable {

    public BeerWithNameAlreadyExistException(String string) {
        super(string);
    }

}
