package com.example.beer_project.exceptions;

public class RatingRangeException extends Exception {

    public RatingRangeException(String string) {
        super(string);
    }

}
