package com.example.beer_project.exceptions;

public class PasswordOutOfRangeException extends Throwable {
    public PasswordOutOfRangeException(String string) {
        super(string);
    }
}
