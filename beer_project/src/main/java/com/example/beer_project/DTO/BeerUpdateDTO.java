package com.example.beer_project.DTO;

public class BeerUpdateDTO {

    private String description;
    private String picture;
    private int abv;
    private String brewery;

    public BeerUpdateDTO() {
    }

    public BeerUpdateDTO(String description, String picture, int abv, String brewery) {
        this.description = description;
        this.picture = picture;
        this.abv = abv;
        this.brewery = brewery;
    }

    public String getDescription() {
        return description;
    }

    public String getPicture() {
        return picture;
    }

    public int getAbv() {
        return abv;
    }

    public String getBrewery() {
        return brewery;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public void setPicture(String picture) {
        this.picture = picture;
    }

    public void setAbv(int abv) {
        this.abv = abv;
    }

    public void setBrewery(String brewery) {
        this.brewery = brewery;
    }
}
