package com.example.beer_project.DTO;

public class BeerDTO {

    private String name;
    private String description;
    private String tag;
    private String picture;
    private String creator;
    private int abv;
    private String brewery;
    private String country;
    private String style;
    private double rating;

    public BeerDTO() {
    }

    public BeerDTO(String name, String description, String tag, String picture, String creator, int abv, String brewery, String country, String style,double rating) {
        this.name = name;
        this.description = description;
        this.tag = tag;
        this.picture = picture;
        this.creator = creator;
        this.abv = abv;
        this.brewery = brewery;
        this.country = country;
        this.style = style;
        this.rating=rating;
    }

    public String getName() {
        return name;
    }

    public String getDescription() {
        return description;
    }

    public String getTag() {
        return tag;
    }

    public String getPicture() {
        return picture;
    }

    public String getCreator() {
        return creator;
    }

    public int getAbv() {
        return abv;
    }

    public String getBrewery() {
        return brewery;
    }

    public String getCountry() {
        return country;
    }

    public String getStyle() {
        return style;
    }

    public double getRating() {
        return rating;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public void setTag(String tag) {
        this.tag = tag;
    }

    public void setPicture(String picture) {
        this.picture = picture;
    }

    public void setCreator(String creator) {
        this.creator = creator;
    }

    public void setAbv(int abv) {
        this.abv = abv;
    }

    public void setBrewery(String brewery) {
        this.brewery = brewery;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public void setStyle(String style) {
        this.style = style;
    }

    public void setRating(double rating) {
        this.rating = rating;
    }
}

