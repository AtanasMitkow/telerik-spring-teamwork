package com.example.beer_project.DTO;

public class BeerDisplayDTO {

    private String name;
    private String picture;
    private int abv;
    private long styleID;
    private double rating;
    private String description;
    private String creator;

    public BeerDisplayDTO() {
    }

    public BeerDisplayDTO(String name, String picture, int abv, long styleID, double rating, String description, String creator) {
        this.name = name;
        this.picture = picture;
        this.abv = abv;
        this.styleID = styleID;
        this.rating = rating;
        this.description = description;
        this.creator = creator;
    }

    public String getName() {
        return name;
    }

    public String getPicture() {
        return picture;
    }

    public int getAbv() {
        return abv;
    }

    public long getStyleID() {
        return styleID;
    }

    public double getRating() {
        return rating;
    }

    public String getDescription() {
        return description;
    }

    public String getCreator() {
        return creator;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setPicture(String picture) {
        this.picture = picture;
    }

    public void setAbv(int abv) {
        this.abv = abv;
    }

    public void setStyleID(long styleID) {
        this.styleID = styleID;
    }

    public void setRating(double rating) {
        this.rating = rating;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public void setCreator(String creator) {
        this.creator = creator;
    }
}
