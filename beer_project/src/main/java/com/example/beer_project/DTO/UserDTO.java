package com.example.beer_project.DTO;

public class UserDTO {
    private long id;
    private String username;
    private String email;
    private String role;
    private String firstName;
    private String lastName;
    private String picture;
    private String country;

    public UserDTO(long id, String username, String password, String email, String role, boolean isDeleted, String firstName, String lastName, String picture, String country) {
        this.id = id;
        this.username = username;
        this.email = email;
        this.role = role;
        this.firstName = firstName;
        this.lastName = lastName;
        this.picture = picture;
        this.country = country;
    }

    public UserDTO() {

    }

    public long getId() {
        return id;
    }

    public String getUsername() {
        return username;
    }

    public String getEmail() {
        return email;
    }

    public String getRole() {
        return role;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public String getPicture() {
        return picture;
    }

    public String getCountry() {
        return country;
    }

    public void setId(long id) {
        this.id = id;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public void setRole(String role) {
        this.role = role;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public void setPicture(String picture) {
        this.picture = picture;
    }

    public void setCountry(String country) {
        this.country = country;
    }
}
